package pwr.bank.service;

import java.util.List;

public interface PozyczkaService {
    List<String> getRodzajePozyczek();
    List<String> getRachunkiKlienta();
}
