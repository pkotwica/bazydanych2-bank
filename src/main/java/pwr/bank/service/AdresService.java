package pwr.bank.service;

import pwr.bank.model.Adres;

import java.util.List;

public interface AdresService {
    List<String> getRachunkiOfKlient(String rachunekNumber);

    Adres findAddressByNameAndOwner(String name, String rachunekNumber);
}
