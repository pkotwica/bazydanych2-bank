package pwr.bank.service;

import pwr.bank.model.Klient;
import pwr.bank.model.Konto;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Optional;

public interface KontoService {
    //return if login and pass are proper
    boolean logIn (String login, String password, List<Integer> ids);

    //return true if login is not in database
    boolean isAvailableLogin(String login);

    //create account and return id of new account
    int register(String login, String password) throws NoSuchAlgorithmException, InvalidKeySpecException;

    //return account of added rachunek
    Optional<Konto> incrementAmountOfRachunki(String klientId);

    void decrementAmountOfRachunki(int kontoID);

    boolean isVerify(String login);

    boolean updateExistingKonto(Konto konto);

    Konto getKontoByKlient(Klient klient);

    boolean verifyByKlient(Klient klient);
}
