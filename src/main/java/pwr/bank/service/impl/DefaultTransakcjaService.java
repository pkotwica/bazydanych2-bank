package pwr.bank.service.impl;

import pwr.bank.dao.tables.TransakcjaDao;
import pwr.bank.model.Konto;
import pwr.bank.model.Transakcja;
import pwr.bank.service.KontoService;
import pwr.bank.service.RachunekService;
import pwr.bank.service.TransakcjaService;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class DefaultTransakcjaService implements TransakcjaService {

    TransakcjaDao transakcjaDao;
    RachunekService rachunekService;
    String RODZAJ_SZYBKI = "Szybki";
    String RODZAJ_ZWYKLY = "Zwykly";

    public DefaultTransakcjaService() {
        this.transakcjaDao = new TransakcjaDao();
        this.rachunekService = new DefaultRachunekService();
    }

    @Override
    public List<Transakcja> getInTransactionByRachunekNumber(String rachunekNumber) {
        return transakcjaDao.getAll().stream().filter(t -> t.getNumer_rachunku_adresata().equals(rachunekNumber)).collect(Collectors.toList());
    }

    @Override
    public List<Transakcja> getOutTransactionByRachunekNumber(String rachunekNumber) {
        return transakcjaDao.getAll().stream().filter(t -> t.getNumer_rachunku_nadawcy().equals(rachunekNumber)).collect(Collectors.toList());
    }

    @Override
    public boolean makeTransaction(String title, String address, String amount, String kindOfTransaction, String accountNumber, String nadawca) {
        try{
            BigDecimal amountGigDecimal = BigDecimal.valueOf(Float.parseFloat(amount));
            String kind;
            if(kindOfTransaction.contains(RODZAJ_SZYBKI)){
                kind = RODZAJ_SZYBKI;
                amountGigDecimal = amountGigDecimal.add(BigDecimal.valueOf(10.00));
            }else
                kind = RODZAJ_ZWYKLY;


            transakcjaDao.add(new Transakcja(0, title, Timestamp.valueOf(LocalDateTime.now()), address, amountGigDecimal, kind, nadawca, accountNumber));
            if(!nadawca.equals("00000000000000000000000000"))
                rachunekService.subMoney(nadawca, amountGigDecimal);
            rachunekService.addMoney(accountNumber, amountGigDecimal);

            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }


}
