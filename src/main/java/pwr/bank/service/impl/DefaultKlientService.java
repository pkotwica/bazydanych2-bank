package pwr.bank.service.impl;

import javafx.scene.input.DataFormat;
import pwr.bank.dao.tables.KlientDao;
import pwr.bank.dao.tables.KontoDao;
import pwr.bank.dao.tables.RachunekDao;
import pwr.bank.model.Klient;
import pwr.bank.model.Konto;
import pwr.bank.model.Pozyczka;
import pwr.bank.model.Rachunek;
import pwr.bank.service.KlientService;
import pwr.bank.service.KontoService;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DefaultKlientService implements KlientService {

    private KlientDao klientDao;
    private KontoDao kontoDao;
    private KontoService kontoService;
    private RachunekDao rachunekDao;

    public DefaultKlientService() {

        this.kontoService = new DefaultKontoService();
        this.klientDao = new KlientDao();
        this.kontoDao = new KontoDao();
        this.rachunekDao = new RachunekDao();
    }


    @Override
    public boolean logIn(String login, String password) {
        List<Integer> ids = klientDao.getAll().stream().map(Klient::getId_konto).collect(Collectors.toList());
        return kontoService.logIn(login, password, ids);
    }

    @Override
    public boolean register(String pesel, String imie, String nazwisko, String numerDowodu, String dataDowoduDD, String dataDowoduMM, String dataDowoduYY, String dataWaznosciDowoduDD, String dataWaznosciDowoduMM, String dataWaznosciDowoduYY, String organWydaniaDowodu, String login, String haslo) throws InvalidKeySpecException, NoSuchAlgorithmException {
        Klient klient = new Klient();
        klient.setId_pesel(pesel);
        klient.setImie(imie);
        klient.setNazwisko(nazwisko);
        klient.setNumer_dowodu(numerDowodu);
        klient.setOrgan_wydania_dowodu(organWydaniaDowodu);

        Date dataWydania = setDate(dataDowoduDD, dataDowoduMM, dataDowoduYY);
        Date dataWaznosci = setDate(dataWaznosciDowoduDD, dataWaznosciDowoduMM, dataWaznosciDowoduYY);

        if (dataWydania == null || dataWaznosci == null)
            return false;

        klient.setData_wydania_dowodu(dataWydania);
        klient.setData_waznosci_dowodu(dataWaznosci);
        klient.setId_konto(kontoService.register(login, haslo));
        klientDao.add(klient);
        return true;
    }

    @Override
    public boolean zaciaganiePozyczki(String kwota, String dataRealizacji, String terminSplatyPozyczki, String RachunekPozyczki, String rodzajPozyczki) throws InvalidKeySpecException, NoSuchAlgorithmException {
//        Pozyczka pozyczka = new Pozyczka();
//        pozyczka.setId(0);
//        pozyczka.setKwota(new BigDecimal(kwota));
//        Timestamp timeStamp = Timestamp.valueOf(LocalDateTime.now());
//        pozyczka.setData_realizacji(timeStamp);
//        Timestamp timeStamp2 = Timestamp.valueOf(LocalDateTime.now());
//        pozyczka.setData_przewidywanej_splaty();
        return true;
    }

    @Override
    public Klient getKlientByAccountLogin(String login) {
        Optional<Integer> accountId = kontoDao.getAll().stream().filter(k -> k.getLogin().equals(login)).map(Konto::getId).findAny();
        return klientDao.getAll().stream().filter(k -> k.getId_konto() == accountId.orElse(0)).findAny().get();
    }

    @Override
    public List<String> getRachunkiOfKlient(String klientId) {
        int idOfAccount = klientDao.getAll().stream().filter(k -> k.getId_pesel().equals(klientId)).map(Klient::getId_konto).findAny().get();
        return rachunekDao.getAll().stream().filter(r -> r.getId_konto() == idOfAccount).map(Rachunek::getId).collect(Collectors.toList());
    }

    @Override
    public List<String> getKlientsAsAStringToManage() {
        List<String> listOfManagmentStringUsers = new ArrayList<>();
        klientDao.getAll().forEach(k -> listOfManagmentStringUsers.add(k.getId_pesel() + " " + k.getImie() + " " + k.getNazwisko()));
        return listOfManagmentStringUsers;
    }

    @Override
    public Klient getKlientToManageByManagmentString(String userAsManagmentString) {
        String klientDataFromString[] = userAsManagmentString.split(" ");
        String klientsPesel = klientDataFromString[0];
        String klientsName = klientDataFromString[1];
        String klientsSurname = klientDataFromString[2];

        return klientDao.getOne(klientsPesel).orElse(Klient.builder().id_pesel("ERR").build());
    }

    @Override
    public boolean isVerify(String id_Pesel) {
        Klient klient = klientDao.getOne(id_Pesel).orElse(Klient.builder().id_konto(-1).build());
        Konto konto = kontoDao.getOne(klient.getId_konto()).orElse(Konto.builder().login("ERR").build());
        return kontoService.isVerify(konto.getLogin());
    }

    @Override
    public boolean updateExistingKlient(Klient klient) {
        try {
            klientDao.update(klient);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private Date setDate(String dds, String mms, String yys) {
        int dd, mm, yy;

        try {
            dd = Integer.parseInt(dds);
            mm = Integer.parseInt(mms);
            yy = Integer.parseInt(yys);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        if (!(dd > 0 && dd < 32 && mm > 0 && mm < 32 && yy > 1950 && yy < 2100))
            return null;

        return Date.valueOf(yys + "-" + mms + "-" + dds);
    }
}
