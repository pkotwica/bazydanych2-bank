package pwr.bank.service.impl;

import pwr.bank.dao.tables.PracownikDao;
import pwr.bank.model.Pracownik;
import pwr.bank.service.KontoService;
import pwr.bank.service.PracownikService;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultPracownikService implements PracownikService {

    private PracownikDao pracownikDao;
    private KontoService kontoService;

    public DefaultPracownikService() {
        this.pracownikDao = new PracownikDao();
        this.kontoService = new DefaultKontoService();
    }

    @Override
    public boolean logIn(String login, String password) {
        List<Integer> ids = pracownikDao.getAll().stream().map(Pracownik::getId_konto).collect(Collectors.toList());
        return kontoService.logIn(login, password, ids);
    }


}
