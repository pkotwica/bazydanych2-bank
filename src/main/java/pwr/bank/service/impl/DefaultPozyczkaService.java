package pwr.bank.service.impl;

import pwr.bank.dao.tables.PozyczkaDao;
import pwr.bank.dao.tables.RodzajPozyczkiDao;
import pwr.bank.model.RodzajPozyczki;
import pwr.bank.service.PozyczkaService;

import java.util.ArrayList;
import java.util.List;

public class DefaultPozyczkaService implements PozyczkaService {
    private RodzajPozyczkiDao rodzajPozyczkiDao;

    public DefaultPozyczkaService() {
        this.rodzajPozyczkiDao = new RodzajPozyczkiDao();
    }

    @Override
    public List<String> getRodzajePozyczek() {
        List<String> listOfManagmentStringRodzajePozyczek = new ArrayList<>();
        rodzajPozyczkiDao.getAll().forEach(p -> listOfManagmentStringRodzajePozyczek.add(p.getNazwa() + " " + p.getOprocentowanie()));
        return listOfManagmentStringRodzajePozyczek;
    }

    @Override
    public List<String> getRachunkiKlienta() {

        return null;
    }
}
