package pwr.bank.service.impl;

import pwr.bank.dao.tables.KlientDao;
import pwr.bank.dao.tables.KontoDao;
import pwr.bank.dao.tables.RachunekDao;
import pwr.bank.model.Klient;
import pwr.bank.model.Konto;
import pwr.bank.model.Rachunek;
import pwr.bank.model.Transakcja;
import pwr.bank.service.KlientService;
import pwr.bank.service.KontoService;
import pwr.bank.service.RachunekService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class DefaultRachunekService implements RachunekService {

    KontoService kontoService;
    RachunekDao rachunekDao;
    KlientDao klientDao;
    KontoDao kontoDao;

    public DefaultRachunekService() {
        this.kontoService = new DefaultKontoService();
        this.rachunekDao = new RachunekDao();
        this.klientDao = new KlientDao();
        this.kontoDao = new KontoDao();
    }

    @Override
    public Optional<Rachunek> createNewRachunekForId(String klientId) {

        if(isPlaceForNewRachunek(klientId)){
            Optional<Konto> konto = kontoService.incrementAmountOfRachunki(klientId);

            String newAccountNumber = generateRachunekNumber();
            konto.ifPresent(k -> rachunekDao.add(new Rachunek(newAccountNumber, BigDecimal.ZERO, k.getId())));

            return Optional.of(Rachunek.builder().id(newAccountNumber).saldo(BigDecimal.ZERO).id_konto(konto.get().getId()).build());
        }

        return Optional.empty();
    }

    @Override
    public boolean isPlaceForNewRachunek(String klientId) {
        Klient klient = klientDao.getAll().stream().filter(k -> k.getId_pesel().equals(klientId)).findAny().get();
        Konto konto = kontoDao.getAll().stream().filter(k -> k.getId() == klient.getId_konto()).findAny().get();

        return konto.getLiczba_rachunkow() < 10;
    }

    @Override
    public BigDecimal getSaldoByRachunekNumber(String rachunekNumber) {
        return rachunekDao.getAll().stream().filter(r -> r.getId().equals(rachunekNumber)).map(Rachunek::getSaldo).findAny().orElse(BigDecimal.ZERO);
    }

    @Override
    public boolean checkEnoughMoney(String rachunekNumber, String amount) {
        Rachunek rachunek = rachunekDao.getAll().stream().filter(r -> r.getId().equals(rachunekNumber)).findAny().get();

        int result = rachunek.getSaldo().compareTo(BigDecimal.valueOf(Float.parseFloat(amount)));

        return result >= 0;
    }

    @Override
    public void addMoney(String accountNumber, BigDecimal amount) {
        Optional<Rachunek> rachunek = rachunekDao.getAll().stream().filter(r -> r.getId().equals(accountNumber)).findAny();

        rachunek.ifPresent(r -> {
            BigDecimal newAmount = r.getSaldo().add(amount);
            r.setSaldo(newAmount);

            rachunekDao.update(r);
        });
    }

    @Override
    public void subMoney(String accountNumber, BigDecimal amount) {
        Optional<Rachunek> rachunek = rachunekDao.getAll().stream().filter(r -> r.getId().equals(accountNumber)).findAny();
        rachunek.ifPresent(r -> {
            BigDecimal newAmount = r.getSaldo().subtract(amount);
            r.setSaldo(newAmount);

            rachunekDao.update(r);
        });
    }

    @Override
    public void deleteRahunekByNumber(String deletingNumberAccount, String targetAccountForMoney) {
        Rachunek deletingRachunek = rachunekDao.getOne(deletingNumberAccount).orElse(new Rachunek());
        addMoney(targetAccountForMoney, deletingRachunek.getSaldo());
        rachunekDao.delete(deletingNumberAccount);
        kontoService.decrementAmountOfRachunki(deletingRachunek.getId_konto());
    }

    @Override
    public void deleteRahunekByNumber(String deletingNumberAccount) {

        rachunekDao.delete(deletingNumberAccount);
    }


    private String generateRachunekNumber() {
        Random random = new Random();
        int MIN = 1000;
        int MAX = 9999;
        String startNumber = "09";
        int subInt1;
        int subInt2;
        int subInt3;
        int subInt4;
        int subInt5;
        int subInt6;
        String rachunekNumber;

        do {
            subInt1 = random.nextInt((MAX - MIN) + 1) + MIN;
            subInt2 = random.nextInt((MAX - MIN) + 1) + MIN;
            subInt3 = random.nextInt((MAX - MIN) + 1) + MIN;
            subInt4 = random.nextInt((MAX - MIN) + 1) + MIN;
            subInt5 = random.nextInt((MAX - MIN) + 1) + MIN;
            subInt6 = random.nextInt((MAX - MIN) + 1) + MIN;

            rachunekNumber = startNumber + String.valueOf(subInt1) + String.valueOf(subInt2) + String.valueOf(subInt3) +
                    String.valueOf(subInt6) + String.valueOf(subInt5) + String.valueOf(subInt4);
        }while(isRepeated(rachunekNumber));

        return rachunekNumber;
    }

    private boolean isRepeated(String rachunekNumber) {
        List<String> numbersOfRachunki = rachunekDao.getAll().stream().map(Rachunek::getId).collect(Collectors.toList());
        return numbersOfRachunki.contains(rachunekNumber);
    }
}
