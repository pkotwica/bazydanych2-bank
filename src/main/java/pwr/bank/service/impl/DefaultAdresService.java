package pwr.bank.service.impl;

import pwr.bank.dao.tables.AdresDao;
import pwr.bank.dao.tables.RachunekDao;
import pwr.bank.model.Adres;
import pwr.bank.model.Rachunek;
import pwr.bank.service.AdresService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultAdresService implements AdresService {
    RachunekDao rachunekDao;
    AdresDao adresDao;

    public DefaultAdresService() {
        this.rachunekDao = new RachunekDao();
        this.adresDao = new AdresDao();
    }

    @Override
    public List<String> getRachunkiOfKlient(String rachunekNumber) {
        Rachunek rachunek = rachunekDao.getAll().stream().filter(r -> r.getId().equals(rachunekNumber)).findAny().get();
        return adresDao.getAll().stream().filter(a -> a.getId_konto() == rachunek.getId_konto()).map(Adres::getNazwa).collect(Collectors.toList());
    }

    @Override
    public Adres findAddressByNameAndOwner(String name, String rachunekNumber) {
        Rachunek rachunek = rachunekDao.getAll().stream().filter(r -> r.getId().equals(rachunekNumber)).findAny().get();
        return adresDao.getAll().stream().filter(a -> a.getId_konto() == rachunek.getId_konto()).filter(a -> a.getNazwa().equals(name)).findAny().get();
    }
}
