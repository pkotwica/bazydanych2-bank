package pwr.bank.service.impl;

import pwr.bank.dao.tables.KlientDao;
import pwr.bank.dao.tables.KontoDao;
import pwr.bank.model.Klient;
import pwr.bank.model.Konto;
import pwr.bank.service.AutoryzacjaService;
import pwr.bank.service.KontoService;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DefaultKontoService implements KontoService, AutoryzacjaService {
    private static int AMOUNT_OF_START_RACHUNKI = 0;
    private static int ACCOUNT_VERYFICATION = 0;

    private KontoDao kontoDao;
    private KlientDao klientDao;

    public DefaultKontoService() {
        this.klientDao = new KlientDao();
        this.kontoDao = new KontoDao();
    }

    @Override
    public boolean logIn(String login, String password, List<Integer> ids) {
        List<Konto> konta = kontoDao.getAll();
        Map<String, String> accounts = konta.stream().filter(a -> ids.contains(a.getId())).collect(Collectors.toMap(Konto::getLogin, Konto::getHaslo));

        //odkodowanie hasla do pozniejszego porownania

        if (accounts.get(login) != null) {
            try {
                AutoryzacjaService.validatePassword(password, accounts.get(login));
                return true;
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                return false;
            }
        } else
            return false;
    }

    @Override
    public boolean isAvailableLogin(String login) {
        List<String> logins = kontoDao.getAll().stream().map(Konto::getLogin).collect(Collectors.toList());
        return !logins.contains(login);
    }

    @Override
    public int register(String login, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        try{
            String strongPassword = AutoryzacjaService.generateStorngPasswordHash(password);

            Konto konto = new Konto();
            konto.setLogin(login);
            konto.setHaslo(strongPassword);
            konto.setWeryfikacja(ACCOUNT_VERYFICATION);
            konto.setLiczba_rachunkow(AMOUNT_OF_START_RACHUNKI);
            kontoDao.add(konto);
        }catch (NoSuchAlgorithmException | InvalidKeySpecException KeySpecEx){}

        return kontoDao.getAll().stream().mapToInt(Konto::getId).max().orElse(0);
    }

    @Override
    public Optional<Konto> incrementAmountOfRachunki(String klientId) {
        Klient klient = klientDao.getAll().stream().filter(k -> k.getId_pesel().equals(klientId)).findAny().get();

        Optional<Konto> konto = kontoDao.getAll().stream().filter(k -> k.getId() == klient.getId_konto()).findAny();

        konto.ifPresent(k -> {
            k.setLiczba_rachunkow(k.getLiczba_rachunkow() + 1);
            kontoDao.update(k);
        });

        return konto;
    }

    @Override
    public void decrementAmountOfRachunki(int kontoID) {
        Optional<Konto> konto = kontoDao.getAll().stream().filter(k -> k.getId() == kontoID).findAny();

        konto.ifPresent(k -> {
            k.setLiczba_rachunkow(k.getLiczba_rachunkow() - 1);
            kontoDao.update(k);
        });
    }

    @Override
    public boolean isVerify(String login) {
        int verification = kontoDao.getAll().stream().filter(k -> k.getLogin().equals(login)).mapToInt(Konto::getWeryfikacja).findAny().orElse(0);
        return verification != 0;
    }

    @Override
    public boolean updateExistingKonto(Konto konto) {
        return kontoDao.update(konto);
    }

    @Override
    public Konto getKontoByKlient(Klient klient) {
        return kontoDao.getOne(klient.getId_konto()).orElse(Konto.builder().login("ERR").build());
    }

    @Override
    public boolean verifyByKlient(Klient klient) {
        Konto konto = kontoDao.getOne(klient.getId_konto()).get();

        try {
            konto.setWeryfikacja(1);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        kontoDao.update(konto);
        return true;
    }
}
