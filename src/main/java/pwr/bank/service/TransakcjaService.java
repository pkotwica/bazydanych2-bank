package pwr.bank.service;

import pwr.bank.model.Transakcja;

import java.util.List;

public interface TransakcjaService {
    List<Transakcja> getInTransactionByRachunekNumber(String rachunekNumber);

    List<Transakcja> getOutTransactionByRachunekNumber(String rachunekNumber);

    boolean makeTransaction(String title, String address, String amount, String kindOfTransaction, String accountNumber, String nadawca);
}
