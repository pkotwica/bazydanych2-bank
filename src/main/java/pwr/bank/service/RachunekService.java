package pwr.bank.service;

import pwr.bank.model.Rachunek;
import pwr.bank.model.Transakcja;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface RachunekService {

    Optional<Rachunek> createNewRachunekForId(String klientId);

    boolean isPlaceForNewRachunek(String klientId);

    BigDecimal getSaldoByRachunekNumber(String rachunekNumber);

    boolean checkEnoughMoney(String rachunekNumber, String amount);

    void addMoney(String accountNumber, BigDecimal amount);

    void subMoney(String accountNumber, BigDecimal amount);

    void deleteRahunekByNumber(String deletingNumberAccount, String targetAccountForMoney);

    void deleteRahunekByNumber(String deletingNumberAccount);
}
