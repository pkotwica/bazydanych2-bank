package pwr.bank.service;

import pwr.bank.model.Klient;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public interface KlientService {

    boolean logIn(String login, String password);

    boolean register(String pesel,
                     String imie,
                     String nazwisko,
                     String numerDowodu,
                     String dataDowoduDD,
                     String dataDowoduMM,
                     String dataDowoduYY,
                     String dataWaznosciDowoduDD,
                     String dataWaznosciDowoduMM,
                     String dataWaznosciDowoduYY,
                     String organWydaniaDowodu,
                     String login,
                     String haslo) throws InvalidKeySpecException, NoSuchAlgorithmException;

    boolean zaciaganiePozyczki(String kwota,
                               String dataRealizacji,
                               String terminSplatyPozyczki,
                               String RachunekPozyczki,
                               String rodzajPozyczki) throws InvalidKeySpecException, NoSuchAlgorithmException;

    Klient getKlientByAccountLogin(String login);

    List<String> getRachunkiOfKlient(String klientId);

    List<String> getKlientsAsAStringToManage();

    Klient getKlientToManageByManagmentString(String userAsManagmentString);

    boolean isVerify(String id_Pesel);

    boolean updateExistingKlient(Klient klient);
}
