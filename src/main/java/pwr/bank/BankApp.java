package pwr.bank;

import pwr.bank.dao.DbConnection;
import pwr.bank.gui.LoginFrame;

import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class BankApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rnd = new Random();

        //tworzenie obiektu singleton DbConnection w celu polaczenia sie z baza danych
        DbConnection db_connection = DbConnection.getInstance();
        db_connection.getConnection();

        //------------------------------------OKIENKA
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame();
            }
        });
    }

}
