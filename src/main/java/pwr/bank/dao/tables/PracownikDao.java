package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Konto;
import pwr.bank.model.Pozyczka;
import pwr.bank.model.Pracownik;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PracownikDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Pracownik pracownik){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO pracownik (IMIE, NAZWISKO, PESEL, NUMER_TELEFONU, ID_KONTO) VALUES (?, ?, ?, ?, ?);");
            preparedStatement.setString(1, pracownik.getImie());
            preparedStatement.setString(2, pracownik.getNazwisko());
            preparedStatement.setString(3, pracownik.getPesel());
            preparedStatement.setString(4, pracownik.getNumer_telefonu());
            preparedStatement.setInt(5, pracownik.getId_konto());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(){
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "UPDATE Adres SET (NR_RACHUNKU, NAZWA, ID_KONTO) VALUES (?, ?, ?);");
//            preparedStatement.setString(1, adres.getNr_rachunku());
//            preparedStatement.setString(2, adres.getNazwa());
//            preparedStatement.setInt(3, adres.getId_konto());
//            preparedStatement.execute();
//        }catch (SQLException e){
//            e.printStackTrace();
//        }
    }

    public void delete(){

    }

    public List<Pracownik> getAll(){
        Pracownik tmpPracownik;
        List<Pracownik> pracownicy = new LinkedList<Pracownik>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_PRACOWNIKA, IMIE, NAZWISKO, PESEL, NUMER_TELEFONU, ID_KONTO FROM pracownik;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpPracownik = new Pracownik();
                tmpPracownik.setId(resultSet.getInt(1));
                tmpPracownik.setImie(resultSet.getString(2));
                tmpPracownik.setNazwisko(resultSet.getString(3));
                tmpPracownik.setPesel(resultSet.getString(4));
                tmpPracownik.setNumer_telefonu(resultSet.getString(5));
                tmpPracownik.setId_konto(resultSet.getInt(6));

                pracownicy.add(tmpPracownik);
            }

            return pracownicy;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pracownicy;
    }

    public Optional<Pracownik> getOne(int id){

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_PRACOWNIKA, IMIE, NAZWISKO, PESEL, NUMER_TELEFONU, ID_KONTO FROM pracownik WHERE ID_PRACOWNIKA = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Pracownik pracownik = new Pracownik();
                pracownik.setId(resultSet.getInt(1));
                pracownik.setImie(resultSet.getString(2));
                pracownik.setNazwisko(resultSet.getString(3));
                pracownik.setPesel(resultSet.getString(4));
                pracownik.setNumer_telefonu(resultSet.getString(5));
                pracownik.setId_konto(resultSet.getInt(6));

                return Optional.of(pracownik);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
