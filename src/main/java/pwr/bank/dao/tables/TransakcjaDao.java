package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Konto;
import pwr.bank.model.Transakcja;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TransakcjaDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Transakcja transakcja){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO transakcja (TYTUL, DATA_REALIZACJI, ADRES, KWOTA, RODZAJ_PRZELEWU, NUMER_RACHUNKU_NADAWCY, NUMER_RACHUNKU_ADRESATA) VALUES (?, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, transakcja.getTytul());
            preparedStatement.setTimestamp(2, transakcja.getData_realizacji());
            preparedStatement.setString(3, transakcja.getAdres());
            preparedStatement.setBigDecimal(4, transakcja.getKwota());
            preparedStatement.setString(5, transakcja.getRodzaj_przelewu());
            preparedStatement.setString(6, transakcja.getNumer_rachunku_nadawcy());
            preparedStatement.setString(7, transakcja.getNumer_rachunku_adresata());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(){
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "UPDATE Adres SET (NR_RACHUNKU, NAZWA, ID_KONTO) VALUES (?, ?, ?);");
//            preparedStatement.setString(1, adres.getNr_rachunku());
//            preparedStatement.setString(2, adres.getNazwa());
//            preparedStatement.setInt(3, adres.getId_konto());
//            preparedStatement.execute();
//        }catch (SQLException e){
//            e.printStackTrace();
//        }
    }

    public void delete(){

    }

    public List<Transakcja> getAll(){
        Transakcja tmpTransakcja;
        List<Transakcja> transakcje = new LinkedList<Transakcja>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_TRANSAKCJI, TYTUL, DATA_REALIZACJI, ADRES, KWOTA, RODZAJ_PRZELEWU, NUMER_RACHUNKU_NADAWCY, NUMER_RACHUNKU_ADRESATA FROM transakcja;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpTransakcja = new Transakcja();
                tmpTransakcja.setId(resultSet.getInt(1));
                tmpTransakcja.setTytul(resultSet.getString(2));
                tmpTransakcja.setData_realizacji(resultSet.getTimestamp(3));
                tmpTransakcja.setAdres(resultSet.getString(4));
                tmpTransakcja.setKwota(resultSet.getBigDecimal(5));
                tmpTransakcja.setAdres(resultSet.getString(6));
                tmpTransakcja.setNumer_rachunku_nadawcy(resultSet.getString(7));
                tmpTransakcja.setNumer_rachunku_adresata(resultSet.getString(8));

                transakcje.add(tmpTransakcja);
            }

            return transakcje;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return transakcje;
    }

    public Optional<Transakcja> getOne(int id){

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_TRANSAKCJI, TYTUL, DATA_REALIZACJI, ADRES, KWOTA, RODZAJ_PRZELEWU, NUMER_RACHUNKU_NADAWCY, NUMER_RACHUNKU_ADRESATA FROM transakcja WHERE ID_TRANSAKCJI = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Transakcja transakcja = new Transakcja();
                transakcja.setId(resultSet.getInt(1));
                transakcja.setTytul(resultSet.getString(2));
                transakcja.setData_realizacji(resultSet.getTimestamp(3));
                transakcja.setAdres(resultSet.getString(4));
                transakcja.setKwota(resultSet.getBigDecimal(5));
                transakcja.setAdres(resultSet.getString(6));
                transakcja.setNumer_rachunku_nadawcy(resultSet.getString(7));
                transakcja.setNumer_rachunku_adresata(resultSet.getString(8));

                return Optional.of(transakcja);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
