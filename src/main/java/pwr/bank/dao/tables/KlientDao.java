package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Klient;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class KlientDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Klient klient){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO klient VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, klient.getId_pesel());
            preparedStatement.setString(2, klient.getImie());
            preparedStatement.setString(3, klient.getNazwisko());
            preparedStatement.setString(4, klient.getNumer_dowodu());
            preparedStatement.setDate(5, klient.getData_wydania_dowodu());
            preparedStatement.setDate(6, klient.getData_waznosci_dowodu());
            preparedStatement.setString(7, klient.getOrgan_wydania_dowodu());
            preparedStatement.setInt(8, klient.getId_konto());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(Klient klient) throws SQLException{
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE Klient SET IMIE = ?, NAZWISKO = ?, NUMER_DOWODU = ?, DATA_WYDANIA_DOWODU = ?, DATA_WAZNOSCI_DOWODU = ?, ORGAN_WYDANIA_DOWODU = ?, ID_KONTO = ? WHERE ID_PESEL = ?;");
            preparedStatement.setString(1, klient.getImie());
            preparedStatement.setString(2, klient.getNazwisko());
            preparedStatement.setString(3, klient.getNumer_dowodu());
            preparedStatement.setDate(4, klient.getData_wydania_dowodu());
            preparedStatement.setDate(5, klient.getData_waznosci_dowodu());
            preparedStatement.setString(6, klient.getOrgan_wydania_dowodu());
            preparedStatement.setInt(7, klient.getId_konto());
            preparedStatement.setString(8, klient.getId_pesel());
            preparedStatement.execute();
    }

    public void delete(){

    }

    public List<Klient> getAll(){
        Klient tmpKlient;
        List<Klient> klienci = new LinkedList<Klient>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_PESEL, IMIE, NAZWISKO, NUMER_DOWODU, DATA_WYDANIA_DOWODU, DATA_WAZNOSCI_DOWODU, ORGAN_WYDANIA_DOWODU, ID_KONTO FROM Klient;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpKlient = new Klient();
                tmpKlient.setId_pesel(resultSet.getString(1));
                tmpKlient.setImie(resultSet.getString(2));
                tmpKlient.setNazwisko(resultSet.getString(3));
                tmpKlient.setNumer_dowodu(resultSet.getString(4));
                tmpKlient.setData_wydania_dowodu(resultSet.getDate(5));
                tmpKlient.setData_waznosci_dowodu(resultSet.getDate(6));
                tmpKlient.setOrgan_wydania_dowodu(resultSet.getString(7));
                tmpKlient.setId_konto(resultSet.getInt(8));

                klienci.add(tmpKlient);
            }

            return klienci;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return klienci;
    }

    public Optional<Klient> getOne(String id_pesel){


        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_PESEL, IMIE, NAZWISKO, NUMER_DOWODU, DATA_WYDANIA_DOWODU, DATA_WAZNOSCI_DOWODU, ORGAN_WYDANIA_DOWODU, ID_KONTO FROM Klient WHERE ID_PESEL = ?;");
            preparedStatement.setString(1, id_pesel);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Klient klient = new Klient();
                klient.setId_pesel(resultSet.getString(1));
                klient.setImie(resultSet.getString(2));
                klient.setNazwisko(resultSet.getString(3));
                klient.setNumer_dowodu(resultSet.getString(4));
                klient.setData_wydania_dowodu(resultSet.getDate(5));
                klient.setData_waznosci_dowodu(resultSet.getDate(6));
                klient.setOrgan_wydania_dowodu(resultSet.getString(7));
                klient.setId_konto(resultSet.getInt(8));

                return Optional.of(klient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
