package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Konto;
import pwr.bank.model.RodzajPozyczki;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class RodzajPozyczkiDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(RodzajPozyczki rodzajPozyczki){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO Rodzaj_Pozyczki (NAZWA, OPROCENTOWANIE) VALUES (?, ?);");
            preparedStatement.setString(1, rodzajPozyczki.getNazwa());
            preparedStatement.setBigDecimal(2, rodzajPozyczki.getOprocentowanie());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(){
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "UPDATE Adres SET (NR_RACHUNKU, NAZWA, ID_KONTO) VALUES (?, ?, ?);");
//            preparedStatement.setString(1, adres.getNr_rachunku());
//            preparedStatement.setString(2, adres.getNazwa());
//            preparedStatement.setInt(3, adres.getId_konto());
//            preparedStatement.execute();
//        }catch (SQLException e){
//            e.printStackTrace();
//        }
    }

    public void delete(){

    }

    public List<RodzajPozyczki> getAll(){
        RodzajPozyczki tmpRodzajPozyczki;
        List<RodzajPozyczki> rodzaje = new LinkedList<RodzajPozyczki>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_RODZAJU_POZYCZKI, NAZWA, OPROCENTOWANIE FROM Rodzaj_Pozyczki;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpRodzajPozyczki = new RodzajPozyczki();
                tmpRodzajPozyczki.setId(resultSet.getInt(1));
                tmpRodzajPozyczki.setNazwa(resultSet.getString(2));
                tmpRodzajPozyczki.setOprocentowanie(resultSet.getBigDecimal(3));

                rodzaje.add(tmpRodzajPozyczki);
            }

            return rodzaje;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rodzaje;
    }

    public Optional<RodzajPozyczki> getOne(int id){

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_RODZAJU_POZYCZKI, NAZWA, OPROCENTOWANIE FROM Rodzaj_Pozyczki WHERE ID_RODZAJU_POZYCZKI = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                RodzajPozyczki rodzajPozyczki = new RodzajPozyczki();
                rodzajPozyczki.setId(resultSet.getInt(1));
                rodzajPozyczki.setNazwa(resultSet.getString(2));
                rodzajPozyczki.setOprocentowanie(resultSet.getBigDecimal(3));

                return Optional.of(rodzajPozyczki);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
