package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Konto;
import pwr.bank.model.Pozyczka;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PozyczkaDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Pozyczka pozyczka){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO pozyczka (KWOTA, DATA_REALIZACJI, DATA_PRZEWIDYWANEJ_SPLATY, NUMER_RACHUNKU, ID_RODZAJU_POZYCZKI) VALUES (?, ?, ?, ?, ?);");
            preparedStatement.setBigDecimal(1, pozyczka.getKwota());
            preparedStatement.setDate(2, (Date) pozyczka.getData_realizacji());
            preparedStatement.setDate(3, (Date) pozyczka.getData_przewidywanej_splaty());
            preparedStatement.setString(4, pozyczka.getNumer_rachunku());
            preparedStatement.setInt(5, pozyczka.getId_rodzaj_pozyczki());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(){
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "UPDATE Adres SET (NR_RACHUNKU, NAZWA, ID_KONTO) VALUES (?, ?, ?);");
//            preparedStatement.setString(1, adres.getNr_rachunku());
//            preparedStatement.setString(2, adres.getNazwa());
//            preparedStatement.setInt(3, adres.getId_konto());
//            preparedStatement.execute();
//        }catch (SQLException e){
//            e.printStackTrace();
//        }
    }

    public void delete(){

    }

    public List<Pozyczka> getAll(){
        Pozyczka tmpPozyczka;
        List<Pozyczka> pozyczki = new LinkedList<Pozyczka>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_POZYCZKI, KWOTA, DATA_REALIZACJI, DATA_PRZEWIDYWANEJ_SPLATY, NUMER_RACHUNKU, ID_RODZAJU_POZYCZKI FROM pozyczka;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpPozyczka = new Pozyczka();
                tmpPozyczka.setId(resultSet.getInt(1));
                tmpPozyczka.setKwota(resultSet.getBigDecimal(2));
                tmpPozyczka.setData_realizacji(resultSet.getDate(3));
                tmpPozyczka.setData_przewidywanej_splaty(resultSet.getDate(4));
                tmpPozyczka.setNumer_rachunku(resultSet.getString(5));
                tmpPozyczka.setId_rodzaj_pozyczki(resultSet.getInt(6));

                pozyczki.add(tmpPozyczka);
            }

            return pozyczki;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pozyczki;
    }

    public Optional<Pozyczka> getOne(int id){

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_POZYCZKI, KWOTA, DATA_REALIZACJI, DATA_PRZEWIDYWANEJ_SPLATY, NUMER_RACHUNKU, ID_RODZAJU_POZYCZKI FROM pozyczka WHERE ID_POZYCZKI = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Pozyczka pozyczka = new Pozyczka();
                pozyczka.setId(resultSet.getInt(1));
                pozyczka.setKwota(resultSet.getBigDecimal(2));
                pozyczka.setData_realizacji(resultSet.getDate(3));
                pozyczka.setData_przewidywanej_splaty(resultSet.getDate(4));
                pozyczka.setNumer_rachunku(resultSet.getString(5));
                pozyczka.setId_rodzaj_pozyczki(resultSet.getInt(6));

                return Optional.of(pozyczka);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
