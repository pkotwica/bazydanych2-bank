package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class AdresDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Adres adres){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO Adres (NR_RACHUNKU, NAZWA, ID_KONTO) VALUES (?, ?, ?);");
            preparedStatement.setString(1, adres.getNr_rachunku());
            preparedStatement.setString(2, adres.getNazwa());
            preparedStatement.setInt(3, adres.getId_konto());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(){
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "UPDATE Adres SET (NR_RACHUNKU, NAZWA, ID_KONTO) VALUES (?, ?, ?);");
//            preparedStatement.setString(1, adres.getNr_rachunku());
//            preparedStatement.setString(2, adres.getNazwa());
//            preparedStatement.setInt(3, adres.getId_konto());
//            preparedStatement.execute();
//        }catch (SQLException e){
//            e.printStackTrace();
//        }
    }

    public void delete(){

    }

    public List<Adres> getAll(){
        Adres tmpAdres;
        List<Adres> adresy = new LinkedList<Adres>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_ADRESU, NR_RACHUNKU, NAZWA,  ID_KONTO FROM Adres;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpAdres = new Adres();
                tmpAdres.setId(resultSet.getInt(1));
                tmpAdres.setNr_rachunku(resultSet.getString(2));
                tmpAdres.setNazwa(resultSet.getString(3));
                tmpAdres.setId_konto(resultSet.getInt(4));

                adresy.add(tmpAdres);
            }

            return adresy;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return adresy;
    }

    public Optional<Adres> getOne(int id){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_ADRESU, NR_RACHUNKU, NAZWA,  ID_KONTO FROM Adres WHERE ID_ADRESU = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Adres adres = new Adres();
                adres.setId(resultSet.getInt(1));
                adres.setNr_rachunku(resultSet.getString(2));
                adres.setNazwa(resultSet.getString(3));
                adres.setId_konto(resultSet.getInt(4));

                return Optional.of(adres);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

}
