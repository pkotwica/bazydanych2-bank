package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Konto;
import pwr.bank.model.Pozyczka;
import pwr.bank.model.Rachunek;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class RachunekDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Rachunek rachunek){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO rachunek VALUES(?, ?, ?);");
            preparedStatement.setString(1, rachunek.getId());
            preparedStatement.setBigDecimal(2, rachunek.getSaldo());
            preparedStatement.setInt(3, rachunek.getId_konto());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void update(Rachunek rachunek){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE Rachunek SET SALDO=? WHERE ID_NUMER_RACHUNKU=?;");
            preparedStatement.setBigDecimal(1, rachunek.getSaldo());
            preparedStatement.setString(2, rachunek.getId());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void delete(String rachunekNumber){
        try {
            PreparedStatement prep = connection.prepareStatement("DELETE FROM Rachunek WHERE ID_NUMER_RACHUNKU=?");
            prep.setString(1, rachunekNumber);
            prep.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Rachunek> getAll(){
        Rachunek tmpRachunek;
        List<Rachunek> rachunki = new LinkedList<Rachunek>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_NUMER_RACHUNKU, SALDO, ID_KONTO FROM rachunek;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpRachunek = new Rachunek();
                tmpRachunek.setId(resultSet.getString(1));
                tmpRachunek.setSaldo(resultSet.getBigDecimal(2));
                tmpRachunek.setId_konto(resultSet.getInt(3));

                rachunki.add(tmpRachunek);
            }

            return rachunki;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rachunki;
    }

    public Optional<Rachunek> getOne(String id){

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_NUMER_RACHUNKU, SALDO, ID_KONTO FROM rachunek WHERE ID_NUMER_RACHUNKU = ?;");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Rachunek rachunek = new Rachunek();
                rachunek.setId(resultSet.getString(1));
                rachunek.setSaldo(resultSet.getBigDecimal(2));
                rachunek.setId_konto(resultSet.getInt(3));

                return Optional.of(rachunek);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
