package pwr.bank.dao.tables;

import pwr.bank.dao.DbConnection;
import pwr.bank.model.Adres;
import pwr.bank.model.Klient;
import pwr.bank.model.Konto;

import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class KontoDao {
    Connection connection = DbConnection.getInstance().getConnection();


    public void add(Konto konto){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO konto (LOGIN, HASLO, WERYFIKACJA, LICZBA_RACHUNKOW) VALUES (?, ?, ?, ?);");
            preparedStatement.setString(1, konto.getLogin());
            preparedStatement.setString(2, konto.getHaslo());
            preparedStatement.setInt(3, konto.getWeryfikacja());
            preparedStatement.setInt(4, konto.getLiczba_rachunkow());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public boolean update(Konto konto){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE Konto SET LOGIN = ?, HASLO = ?, WERYFIKACJA = ?, LICZBA_RACHUNKOW = ? WHERE ID_KONTO = ?;");
            preparedStatement.setString(1, konto.getLogin());
            preparedStatement.setString(2, konto.getHaslo());
            preparedStatement.setInt(3, konto.getWeryfikacja());
            preparedStatement.setInt(4, konto.getLiczba_rachunkow());
            preparedStatement.setInt(5, konto.getId());
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void delete(){

    }

    public List<Konto> getAll(){
        Konto tmpKonto;
        List<Konto> konta = new LinkedList<Konto>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_KONTO, LOGIN, HASLO, WERYFIKACJA, LICZBA_RACHUNKOW FROM konto;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tmpKonto = new Konto();
                tmpKonto.setId(resultSet.getInt(1));
                tmpKonto.setLogin(resultSet.getString(2));
                tmpKonto.setHaslo(resultSet.getString(3));
                tmpKonto.setWeryfikacja(resultSet.getInt(4));
                tmpKonto.setLiczba_rachunkow(resultSet.getInt(5));

                konta.add(tmpKonto);
            }

            return konta;
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public Optional<Konto> getOne(int id){

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT ID_KONTO, LOGIN, HASLO, WERYFIKACJA, LICZBA_RACHUNKOW FROM konto WHERE ID_KONTO = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Konto konto = new Konto();
                konto.setId(resultSet.getInt(1));
                konto.setLogin(resultSet.getString(2));
                konto.setHaslo(resultSet.getString(3));
                konto.setWeryfikacja(resultSet.getInt(4));
                konto.setLiczba_rachunkow(resultSet.getInt(5));

                return Optional.of(konto);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();

        }

        return Optional.empty();
    }
}
