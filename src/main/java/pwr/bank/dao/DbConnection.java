package pwr.bank.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
    private static DbConnection ourInstance = new DbConnection();

    public static DbConnection getInstance() {
        return ourInstance;
    }

    private final static String DRIVER = "com.mysql.cj.jdbc.Driver";
    private final static String DATABASE = "jdbc:mysql://localhost:3306/bank?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT";
    private final static String USER = "banksapp";
    private final static String PASSWORD = "banksapp";

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }


    private DbConnection() {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(DATABASE, USER, PASSWORD);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}



