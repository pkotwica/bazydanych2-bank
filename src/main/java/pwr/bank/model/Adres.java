package pwr.bank.model;

import lombok.*;

@Getter
@Setter
public class Adres {

    private int id;
    private String nr_rachunku;
    private String nazwa;
    private int id_konto;

}
