package pwr.bank.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Transakcja {

    private int id;
    private String tytul;
    private Timestamp data_realizacji;
    private String adres;
    private BigDecimal kwota;
    private String rodzaj_przelewu;
    private String numer_rachunku_nadawcy;
    private String numer_rachunku_adresata;

}
