package pwr.bank.model;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class Pozyczka {

    private int id;
    private BigDecimal kwota;
    private Date data_realizacji;
    private Date data_przewidywanej_splaty;
    private String numer_rachunku;
    private int id_rodzaj_pozyczki;

}
