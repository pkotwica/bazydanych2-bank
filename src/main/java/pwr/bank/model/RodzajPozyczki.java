package pwr.bank.model;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
public class RodzajPozyczki {
    private int id;
    private String nazwa;
    private BigDecimal oprocentowanie;
}
