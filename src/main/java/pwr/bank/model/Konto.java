package pwr.bank.model;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Konto {

    private int id;
    private String login;
    private String haslo;
    private int weryfikacja;
    private int liczba_rachunkow;

}
