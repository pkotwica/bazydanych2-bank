package pwr.bank.model;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Rachunek {

    private String id;
    private BigDecimal saldo;
    private int id_konto;

}
