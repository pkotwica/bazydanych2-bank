package pwr.bank.model;

import java.util.Date;

import lombok.*;

@Getter
@Setter
public class Pracownik {

    private int id;
    private String imie;
    private String nazwisko;
    private String pesel;
    private String numer_telefonu;
    private int id_konto;

}
