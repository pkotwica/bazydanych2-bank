package pwr.bank.model;

import lombok.*;

import java.sql.Date;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Klient {

    private String id_pesel;
    private String imie;
    private String nazwisko;
    private String numer_dowodu;
    private Date data_wydania_dowodu;
    private Date data_waznosci_dowodu;
    private String organ_wydania_dowodu;
    private int id_konto;

}
