package pwr.bank.gui;

import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultPracownikService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginFrame extends JFrame implements ActionListener {
    private JButton loginButton, registerButton;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JLabel loginLabel, passwordLabel, workerLabel;
    private JCheckBox isWorker;

    public LoginFrame() {
        super("LOGOWANIE");
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(400, 300);
        setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        loginLabel = new JLabel("LOGIN: ");
        loginLabel.setBounds(50, 70, 100, 20);
        add(loginLabel);

        passwordLabel = new JLabel("HASLO: ");
        passwordLabel.setBounds(50, 100, 100, 20);
        add(passwordLabel);

        workerLabel = new JLabel("pracownik: ");
        workerLabel.setBounds(50, 130, 100, 20);
        add(workerLabel);

        loginField = new JTextField("");
        loginField.setBounds(150, 70, 200, 20);
        add(loginField);

        passwordField = new JPasswordField("");
        passwordField.setBounds(150, 100, 200, 20);
        add(passwordField);

        loginButton = new JButton("ZALOGUJ");
        loginButton.setBounds(50, 160, 120, 20);
        loginButton.addActionListener(this::actionPerformed);
        add(loginButton);

        registerButton = new JButton("ZAREJESTRUJ");
        registerButton.setBounds(190, 160, 120, 20);
        registerButton.addActionListener(this::actionPerformed);
        add(registerButton);

        isWorker = new JCheckBox("Pracownik");
        isWorker.setBounds(150, 130, 20, 20);
        add(isWorker);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == loginButton) {
            if (isWorker.isSelected()) {
                DefaultPracownikService pracownikService = new DefaultPracownikService();

                if (pracownikService.logIn(loginField.getText(), passwordField.getText()))
                    openPracownikStartFrame(loginField.getText());
                else
                    JOptionPane.showMessageDialog(
                            null,
                            "Nieprawidlowy Login lub Haslo",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);


            } else {
                DefaultKlientService klientService = new DefaultKlientService();

                if (klientService.logIn(loginField.getText(), passwordField.getText()))
                    openKlientStartFrame(loginField.getText());
                else
                    JOptionPane.showMessageDialog(
                            null,
                            "Nieprawidlowy Login lub Haslo",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
            }
        } else {
            this.setVisible(false);
            RegisterFrame registerFrame = new RegisterFrame(this);
        }
    }

    private void openKlientStartFrame(String login) {
        this.dispose();
        KlientStartFrame klientStartFrame = new KlientStartFrame(login, new Point(this.getLocation()));
    }

    private void openPracownikStartFrame(String login) {
        this.dispose();
        PracownikStartFrame pracownikStartFrame = new PracownikStartFrame(login, new Point(this.getLocation()));
    }


}
