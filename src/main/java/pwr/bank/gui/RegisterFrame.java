package pwr.bank.gui;

import pwr.bank.service.KlientService;
import pwr.bank.service.KontoService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultKontoService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class RegisterFrame extends JFrame implements ActionListener {

    private KlientService klientService;
    private KontoService kontoService;

    private JFrame loginFrame;

    private JLabel peselLabel,
            imieLabel,
            nazwiskoLabel,
            numerDowoduLabel,
            dataDowodLabel,
            dataWaznosciDowoduLabel,
            organWydaniaDowoduLabel,
            loginLabel,
            hasloLabel,
            hasloRepeatLabel;

    private JTextField peselText,
            imieText,
            nazwiskoText,
            numerDowoduText,
            dataDowodTextDD,
            dataDowodTextMM,
            dataDowodTextYY,
            dataWaznosciDowoduTextDD,
            dataWaznosciDowoduTextMM,
            dataWaznosciDowoduTextYY,
            organWydaniaDowoduText,
            loginText;

    private JPasswordField hasloText, hasloRepeatText;

    private JButton registerButton, goBackButton;

    RegisterFrame(JFrame loginFrame) {
        super("REJESTRACJA");

        this.loginFrame = loginFrame;
        this.klientService = new DefaultKlientService();
        this.kontoService = new DefaultKontoService();

        setSize(400, 800);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        peselLabel = new JLabel("PESEL: ");
        peselLabel.setBounds(50, 70, 200, 20);
        add(peselLabel);

        peselText = new JTextField("");
        peselText.setBounds(150, 100, 200, 20);
        add(peselText);

        imieLabel = new JLabel("IMIE: ");
        imieLabel.setBounds(50, 130, 200, 20);
        add(imieLabel);

        imieText = new JTextField("");
        imieText.setBounds(150, 160, 200, 20);
        add(imieText);

        nazwiskoLabel = new JLabel("NAZWISKO: ");
        nazwiskoLabel.setBounds(50, 190, 200, 20);
        add(nazwiskoLabel);

        nazwiskoText = new JTextField("");
        nazwiskoText.setBounds(150, 220, 200, 20);
        add(nazwiskoText);

        numerDowoduLabel = new JLabel("NUMER DOWODU: ");
        numerDowoduLabel.setBounds(50, 250, 200, 20);
        add(numerDowoduLabel);

        numerDowoduText = new JTextField("");
        numerDowoduText.setBounds(150, 280, 200, 20);
        add(numerDowoduText);

        dataDowodLabel = new JLabel("DATA WYDANIA DOWODU: ");
        dataDowodLabel.setBounds(50, 310, 200, 20);
        add(dataDowodLabel);

        dataDowodTextDD = new JTextField("DD");
        dataDowodTextDD.setBounds(80, 340, 40, 20);
        add(dataDowodTextDD);
        dataDowodTextMM = new JTextField("MM");
        dataDowodTextMM.setBounds(130, 340, 40, 20);
        add(dataDowodTextMM);
        dataDowodTextYY = new JTextField("RRRR");
        dataDowodTextYY.setBounds(170, 340, 100, 20);
        add(dataDowodTextYY);

        dataWaznosciDowoduLabel = new JLabel("DATA WAZNOSCI DOWODU: ");
        dataWaznosciDowoduLabel.setBounds(50, 370, 200, 20);
        add(dataWaznosciDowoduLabel);

        dataWaznosciDowoduTextDD = new JTextField("DD");
        dataWaznosciDowoduTextDD.setBounds(80, 400, 40, 20);
        add(dataWaznosciDowoduTextDD);
        dataWaznosciDowoduTextMM = new JTextField("MM");
        dataWaznosciDowoduTextMM.setBounds(130, 400, 40, 20);
        add(dataWaznosciDowoduTextMM);
        dataWaznosciDowoduTextYY = new JTextField("RRRR");
        dataWaznosciDowoduTextYY.setBounds(170, 400, 100, 20);
        add(dataWaznosciDowoduTextYY);

        organWydaniaDowoduLabel = new JLabel("ORGAN WYDANIA DOWODU: ");
        organWydaniaDowoduLabel.setBounds(50, 430, 200, 20);
        add(organWydaniaDowoduLabel);

        organWydaniaDowoduText = new JTextField("");
        organWydaniaDowoduText.setBounds(150, 460, 200, 20);
        add(organWydaniaDowoduText);

        loginLabel = new JLabel("LOGIN: ");
        loginLabel.setBounds(50, 490, 200, 20);
        add(loginLabel);

        loginText = new JTextField("");
        loginText.setBounds(150, 520, 200, 20);
        add(loginText);

        hasloLabel = new JLabel("HASLO: ");
        hasloLabel.setBounds(50, 550, 200, 20);
        add(hasloLabel);

        hasloText = new JPasswordField("");
        hasloText.setBounds(150, 580, 200, 20);
        add(hasloText);

        hasloRepeatLabel = new JLabel("POWTORZ HASLO: ");
        hasloRepeatLabel.setBounds(50, 610, 200, 20);
        add(hasloRepeatLabel);

        hasloRepeatText = new JPasswordField("");
        hasloRepeatText.setBounds(150, 640, 200, 20);
        add(hasloRepeatText);

        registerButton = new JButton("ZAREJESTRUJ");
        registerButton.setBounds(50, 700, 150, 20);
        registerButton.addActionListener(this::actionPerformed);
        add(registerButton);

        goBackButton = new JButton("ANULUJ");
        goBackButton.setBounds(210, 700, 150, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(registerButton)) {
            if (peselText.getText().isEmpty() &&
                    imieText.getText().isEmpty() &&
                    nazwiskoText.getText().isEmpty() &&
                    numerDowoduText.getText().isEmpty() &&
                    organWydaniaDowoduText.getText().isEmpty() &&
                    loginText.getText().isEmpty() &&
                    hasloText.getText().isEmpty() &&
                    hasloRepeatText.getText().isEmpty()) {

                JOptionPane.showMessageDialog(
                        null,
                        "Wszystkie pola wymagaja uzupelnienia!",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                if (!kontoService.isAvailableLogin(loginText.getText())) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Podany login juz istnieje!",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    if (!hasloText.getText().equals(hasloRepeatText.getText())) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Powtorz poprawnie haslo!",
                                "ERROR",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        boolean registrationResult = false;
                        try {
                            registrationResult = klientService.register(peselText.getText(),
                                    imieText.getText(),
                                    nazwiskoText.getText(),
                                    numerDowoduText.getText(),
                                    dataDowodTextDD.getText(),
                                    dataDowodTextMM.getText(),
                                    dataDowodTextYY.getText(),
                                    dataWaznosciDowoduTextDD.getText(),
                                    dataWaznosciDowoduTextMM.getText(),
                                    dataWaznosciDowoduTextYY.getText(),
                                    organWydaniaDowoduText.getText(),
                                    loginText.getText(),
                                    hasloText.getText());
                        } catch (InvalidKeySpecException e1) {
                            e1.printStackTrace();
                        } catch (NoSuchAlgorithmException e1) {
                            e1.printStackTrace();
                        }

                        if (!registrationResult) {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Nie udalo sie stworzyc uzytkownika.\nCos poszlo nie tak.",
                                    "ERROR",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                        loginFrame.setVisible(true);
                        this.dispose();
                    }
                }
            }
        }

        if (e.getSource().equals(goBackButton)) {
            loginFrame.setVisible(true);
            this.dispose();
        }

    }


}
