package pwr.bank.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PracownikStartFrame extends JFrame implements ActionListener {


    private JLabel greatingLabel;
    private JButton usersButton, pozyczkiButton, logOutButton;

    public PracownikStartFrame(String login, Point point) {
        super("PRACOWNIK");
        setLocation(point);
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        greatingLabel = new JLabel("ADMINISTARCJA");
        greatingLabel.setBounds(50, 30, 100, 20);
        add(greatingLabel);

        usersButton = new JButton("ZARZADZAJ UZYTKOWNIKAMI");
        usersButton.setBounds(25, 80, 250, 20);
        usersButton.addActionListener(this::actionPerformed);
        add(usersButton);


        logOutButton = new JButton("WYLOGUJ");
        logOutButton.setBounds(25, 140, 250, 20);
        logOutButton.addActionListener(this::actionPerformed);
        add(logOutButton);


        setVisible(true);
    }

    public void actionPerformed(ActionEvent actionEvent) {

        if(actionEvent.getSource().equals(usersButton)){
            UsersManagmentFrame usersManagmentFrame = new UsersManagmentFrame(this, this.getLocation());
            this.setVisible(false);
        }


        if(actionEvent.getSource().equals(logOutButton)){
            LoginFrame loginFrame = new LoginFrame();
            this.dispose();
        }
    }
}
