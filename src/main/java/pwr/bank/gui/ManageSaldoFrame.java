package pwr.bank.gui;

import pwr.bank.model.Klient;
import pwr.bank.service.KlientService;
import pwr.bank.service.RachunekService;
import pwr.bank.service.TransakcjaService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultRachunekService;
import pwr.bank.service.impl.DefaultTransakcjaService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ManageSaldoFrame extends JFrame implements ActionListener {

    private String GET_OUT_MONEY_DECISION = "Wyplata";
    private String GET_IN_MONEY_DECISION = "Wplata";
    private String MOVE_MONEY_DECISION = "Przelew";
    private String BANK_ADDRESS= "Wroclaw, ul.Pomorska 97";

    private TransakcjaService transakcjaService;
    private RachunekService rachunekService;
    private KlientService klientService;
    private JFrame manageUserFrame;

    private JLabel decisionLabel, accountNumberLabel, titleTransactionLabel, priceLabel, saldoLabel, addressLabel;
    private JTextField accountNumberField, titleTransactionField, priceField, addressField;
    private JComboBox<String> decisionComboBox, rachunkiComboBox;
    private JButton makeTransactionButton, goBackButton;

    public ManageSaldoFrame(JFrame manageUserFrame, Point point, Klient klient) {
        super("TRANSAKCJE");
        setLocation(point);
        setSize(300, 460);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);


        this.transakcjaService = new DefaultTransakcjaService();
        this.rachunekService = new DefaultRachunekService();
        this.manageUserFrame = manageUserFrame;
        this.klientService = new DefaultKlientService();
        List<String> decisions = new ArrayList<>(Arrays.asList(GET_OUT_MONEY_DECISION, GET_IN_MONEY_DECISION, MOVE_MONEY_DECISION));

        saldoLabel = new JLabel("SALDO: ");
        saldoLabel.setBounds(20, 30, 250, 20);
        add(saldoLabel);

        decisionLabel = new JLabel("Wybierz rodzaj transakcji: ");
        decisionLabel.setBounds(20, 60, 250, 20);
        add(decisionLabel);

        accountNumberLabel = new JLabel("Numer konta: " );
        accountNumberLabel.setBounds(20, 120, 100, 20);
        add(accountNumberLabel);

        accountNumberField = new JTextField("");
        accountNumberField.setBounds(20, 150, 200, 20);
        add(accountNumberField);

        titleTransactionLabel = new JLabel("Tytul transakcji: " );
        titleTransactionLabel.setBounds(20, 180, 100, 20);
        add(titleTransactionLabel);

        titleTransactionField = new JTextField("");
        titleTransactionField.setBounds(20, 210, 200, 20);
        add(titleTransactionField);

        addressLabel = new JLabel("ADRES: ");
        addressLabel.setBounds(20, 240, 200, 20);
        add(addressLabel);

        addressField = new JTextField("");
        addressField.setBounds(20, 270, 200, 20);
        add(addressField);

        priceLabel = new JLabel("Kwota (np. 10.l1): " );
        priceLabel.setBounds(20, 300, 200, 20);
        add(priceLabel);

        priceField = new JTextField("");
        priceField.setBounds(20, 330, 200, 20);
        add(priceField);

        makeTransactionButton = new JButton("WYKONAJ TRANSAKCJE");
        makeTransactionButton.setBounds(50, 370, 200, 20);
        makeTransactionButton.addActionListener(this::actionPerformed);
        add(makeTransactionButton);

        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(50, 400, 200, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        decisionComboBox = new JComboBox<>();
        decisionComboBox.setBounds(10, 90, 220, 20);
        decisionComboBox.addActionListener(this::actionPerformed);
        for (String r : decisions)
            decisionComboBox.addItem(r);
        decisionComboBox.setSelectedIndex(0);
        add(decisionComboBox);

        rachunkiComboBox = new JComboBox<>();
        rachunkiComboBox.setBounds(10, 10, 220, 20);
        rachunkiComboBox.addActionListener(this::actionPerformed);
        List<String> rachunki = klientService.getRachunkiOfKlient(klient.getId_pesel());
        for (String r : rachunki)
            rachunkiComboBox.addItem(r);
        rachunkiComboBox.addItem("");
        rachunkiComboBox.setSelectedIndex(0);
        add(rachunkiComboBox);

        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(makeTransactionButton)){
            boolean result = false;

            if(decisionComboBox.getSelectedItem().toString().equals(GET_IN_MONEY_DECISION)) {
                result = transakcjaService.makeTransaction(GET_IN_MONEY_DECISION, BANK_ADDRESS, priceField.getText(), "Zwykly", rachunkiComboBox.getSelectedItem().toString(), "00000000000000000000000000");
            }

            if(decisionComboBox.getSelectedItem().toString().equals(GET_OUT_MONEY_DECISION)) {
                result = transakcjaService.makeTransaction(GET_OUT_MONEY_DECISION, BANK_ADDRESS, priceField.getText(), "Zwykly", "BANK", rachunkiComboBox.getSelectedItem().toString());
            }

            if(decisionComboBox.getSelectedItem().toString().equals(MOVE_MONEY_DECISION)) {
                result = transakcjaService.makeTransaction(titleTransactionField.getText(), addressField.getText(), priceField.getText(),"Zwykly", accountNumberField.getText(), rachunkiComboBox.getSelectedItem().toString());
            }

            if(result) {
                JOptionPane.showMessageDialog(
                        null,
                        "Transakcja wykonana pomyslnie.",
                        "TRANSAKCJA",
                        JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(
                        null,
                        "Cos poszlo nie tak.",
                        "TRANSAKCJA",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

        if(e.getSource().equals(decisionComboBox)){
            if(decisionComboBox.getSelectedItem().toString().equals(GET_IN_MONEY_DECISION) || decisionComboBox.getSelectedItem().toString().equals(GET_OUT_MONEY_DECISION)){
                accountNumberField.setEnabled(false);
                titleTransactionField.setEnabled(false);
                addressField.setEnabled(false);
            }


            if(decisionComboBox.getSelectedItem().toString().equals(MOVE_MONEY_DECISION)){
                accountNumberField.setEnabled(true);
                titleTransactionField.setEnabled(true);
                addressField.setEnabled(true);
            }
        }

        if(e.getSource().equals(rachunkiComboBox)){
            if(rachunkiComboBox.getSelectedItem().toString().equals("")){
                saldoLabel.setText("SALDO: -");
            }else{
                saldoLabel.setText("SALDO: " + rachunekService.getSaldoByRachunekNumber((String) rachunkiComboBox.getSelectedItem()).toString());
            }
        }

        if(e.getSource().equals(goBackButton)){
            this.manageUserFrame.setVisible(true);
            this.dispose();
        }
    }
}
