package pwr.bank.gui;

import pwr.bank.model.Klient;
import pwr.bank.service.AdresService;
import pwr.bank.service.KlientService;
import pwr.bank.service.PozyczkaService;
import pwr.bank.service.impl.DefaultAdresService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultPozyczkaService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public class PozyczkiFrame extends JFrame implements ActionListener {

    PozyczkaService pozyczkaService;
    AdresService adresService;
    JFrame klientStartFrame;
    private JButton goBackButton, submitButton;
    private JLabel slectPozyczkiLabel, rachunekLabel, adresyLabel, terminSplatyPozyczkiLabel;
    private JComboBox<String> pozyczkiComboBox, adresyComboBox;
    private JTextField rachunekTextField, terminSplatyPozyczkiTextDD, terminSplatyPozyczkiTextMM, terminSplatyPozyczkiTextYY;
    private Klient klient;
    private KlientService klientService;


    public PozyczkiFrame(JFrame klientStartFrame, Point point, Klient klient) {
        super("POZYCZKI");

        this.klientStartFrame = klientStartFrame;
        this.pozyczkaService = new DefaultPozyczkaService();
        this.adresService = new DefaultAdresService();
        this.klient = klient;
        this.klientService = new DefaultKlientService();

                setLocation(point);
        setSize(550, 290);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        slectPozyczkiLabel = new JLabel("WYBIERZ POZYCZKE: ");
        slectPozyczkiLabel.setBounds(50, 10, 200, 20);
        add(slectPozyczkiLabel);

        pozyczkiComboBox = new JComboBox<>();
        pozyczkiComboBox.setBounds(50, 40, 250, 20);
        List<String> rodzajPozyczek = pozyczkaService.getRodzajePozyczek();
        for (String p : rodzajPozyczek)
            pozyczkiComboBox.addItem(p);
        pozyczkiComboBox.setSelectedIndex(1);
        add(pozyczkiComboBox);

        //Na jaki rach zaciagam pozyczke
        rachunekLabel = new JLabel("NUMER RACHUNKU NA KTORY ZOSTANIE ZACIAGNIETA POZYCZKA: ");
        rachunekLabel.setBounds(50, 70, 400, 20);
        add(rachunekLabel);

        adresyLabel = new JLabel("TWOJE OBECNE RACHUNKI");
        adresyLabel.setBounds(50, 100, 200, 20);
        add(adresyLabel);

        adresyComboBox = new JComboBox<>();
        adresyComboBox.setBounds(250, 100, 170, 20);
        adresyComboBox.addItem("");
        List<String> adresy = klientService.getRachunkiOfKlient(klient.getId_pesel());
        for (String r : adresy)
            adresyComboBox.addItem(r);
        add(adresyComboBox);

        terminSplatyPozyczkiLabel = new JLabel("TERMIN SPLATY POZYCZKI: ");
        terminSplatyPozyczkiLabel.setBounds(50, 130, 200, 20);
        add(terminSplatyPozyczkiLabel);

        terminSplatyPozyczkiTextDD = new JTextField("DD");
        terminSplatyPozyczkiTextDD.setBounds(50, 160, 40, 20);
        add(terminSplatyPozyczkiTextDD);
        terminSplatyPozyczkiTextMM = new JTextField("MM");
        terminSplatyPozyczkiTextMM.setBounds(100, 160, 40, 20);
        add(terminSplatyPozyczkiTextMM);
        terminSplatyPozyczkiTextYY = new JTextField("RRRR");
        terminSplatyPozyczkiTextYY.setBounds(140, 160, 100, 20);
        add(terminSplatyPozyczkiTextYY);

        submitButton = new JButton("ZACIAGNIJ POZYCZKE");
        submitButton.setBounds(50, 200, 220, 20);
        submitButton.addActionListener(this::actionPerformed);
        add(submitButton);

        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(350, 200, 120, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(submitButton)) {
            if (pozyczkiComboBox.getSelectedItem() == null &&
                    adresyComboBox.getSelectedItem() == null &&
                    terminSplatyPozyczkiTextDD.getText().isEmpty() &&
                    terminSplatyPozyczkiTextMM.getText().isEmpty() &&
                    terminSplatyPozyczkiTextYY.getText().isEmpty()) {

                JOptionPane.showMessageDialog(
                        null,
                        "Wszystkie pola wymagaja uzupelnienia!",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            } else {
//                boolean registrationResult = false;
//                try {
//                    registrationResult = klientService.
//                } catch (InvalidKeySpecException e1) {
//                    e1.printStackTrace();
//                } catch (NoSuchAlgorithmException e1) {
//                    e1.printStackTrace();
//                }
            }
        }
        if (e.getSource().equals(goBackButton)) {
            klientStartFrame.setVisible(true);
            this.dispose();
        }
    }
}
