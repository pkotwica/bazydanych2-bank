package pwr.bank.gui;

import pwr.bank.model.Klient;
import pwr.bank.service.KlientService;
import pwr.bank.service.KontoService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultKontoService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KlientStartFrame extends JFrame implements ActionListener {

    private KlientService klientService;
    private KontoService kontoService;

    private String login;
    private Klient klient;

    private JLabel greatingLabel;
    private JButton rachunkiButton, pozyczkiButton, logOutButton;

    public KlientStartFrame(String login, Point point) {
        super("KLIENT");

        this.klientService = new DefaultKlientService();
        this.kontoService = new DefaultKontoService();
        this.login = login;

        klient = klientService.getKlientByAccountLogin(login);

        setLocation(point);
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        greatingLabel = new JLabel("Witaj " + klient.getImie() + "!");
        greatingLabel.setBounds(50, 30, 100, 20);
        add(greatingLabel);

        rachunkiButton = new JButton("PRZEGLADAJ RACHUNKI");
        rachunkiButton.setBounds(50, 80, 200, 20);
        rachunkiButton.addActionListener(this::actionPerformed);
        add(rachunkiButton);

        pozyczkiButton = new JButton("POZYCZKI");
        pozyczkiButton.setBounds(50, 110, 200, 20);
        pozyczkiButton.addActionListener(this::actionPerformed);
        add(pozyczkiButton);

        logOutButton = new JButton("WYLOGUJ");
        logOutButton.setBounds(50, 140, 200, 20);
        logOutButton.addActionListener(this::actionPerformed);
        add(logOutButton);


        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(rachunkiButton)) {

            RachunkiFrame rachunkiFrame = new RachunkiFrame(klient.getId_pesel(), this, new Point(this.getLocation()));
            this.setVisible(false);
        }


        if (e.getSource().equals(pozyczkiButton)) {

            if(kontoService.isVerify(login)) {
                PozyczkiFrame pozyczkiFrame = new PozyczkiFrame(this, new Point(this.getLocation()),klient);
                this.setVisible(false);
            }else {
                JOptionPane.showMessageDialog(
                        null,
                        "Aby uzyskac dostep do pozyczek musisz dokonac\nweryfikacji konta w placowce.",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource().equals(logOutButton)) {
            LoginFrame loginFrame = new LoginFrame();
            this.dispose();
        }
    }
}
