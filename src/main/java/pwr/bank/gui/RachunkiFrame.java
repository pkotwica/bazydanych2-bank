package pwr.bank.gui;

import pwr.bank.dao.tables.TransakcjaDao;
import pwr.bank.model.Rachunek;
import pwr.bank.model.Transakcja;
import pwr.bank.service.KlientService;
import pwr.bank.service.RachunekService;
import pwr.bank.service.TransakcjaService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultRachunekService;
import pwr.bank.service.impl.DefaultTransakcjaService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class RachunkiFrame extends JFrame implements ActionListener {

    private KlientService klientService;
    private TransakcjaService transakcjaService;
    private RachunekService rachunekService;

    private JFrame klientStartFrame;
    private String klientId;

    private JComboBox<String> rachunkiComboBox;
    private JButton nowyRachubekButton, goBackButton, makeTransactionButton;
    private JLabel saldoLabel, saldoValueLabel, transakcjeInLabel, transakcjeOutLabel;
    private JTable inTransactionTable, outTransactionTable;
    private JScrollPane inTransactionScrollPane, outTransactionScrollPane;

    public RachunkiFrame(String id, JFrame klientStartFrame, Point point) {
        super("RACHUNKI");

        this.klientStartFrame = klientStartFrame;
        this.klientService = new DefaultKlientService();
        this.rachunekService = new DefaultRachunekService();
        this.transakcjaService = new DefaultTransakcjaService();
        this.klientId = id;

        setLocation(point);
        setSize(740, 580);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);



        nowyRachubekButton = new JButton("NOWY RACHUNEK");
        nowyRachubekButton.setBounds(250, 10, 140, 20);
        nowyRachubekButton.addActionListener(this::actionPerformed);
        add(nowyRachubekButton);

        makeTransactionButton = new JButton("NOWY PRZELEW");
        makeTransactionButton.setBounds(400, 10, 140, 20);
        makeTransactionButton.addActionListener(this::actionPerformed);
        add(makeTransactionButton);

        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(450, 520, 120, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        saldoLabel = new JLabel("SALDO: ");
        saldoLabel.setBounds(20, 50, 100, 20);
        add(saldoLabel);

        saldoValueLabel = new JLabel("");
        saldoValueLabel.setBounds(150, 50, 100, 20);
        add(saldoValueLabel);

        transakcjeInLabel = new JLabel("TRANSAKCJE PRZYCHODZACE");
        transakcjeInLabel.setBounds(50, 80, 200, 20);
        add(transakcjeInLabel);

        inTransactionTable = new JTable();
        inTransactionScrollPane = new JScrollPane(inTransactionTable);
        inTransactionTable.setPreferredScrollableViewportSize(new Dimension(650, 180));
        inTransactionScrollPane.setBounds(10, 110, 670, 180);
        add(inTransactionScrollPane);

        transakcjeOutLabel = new JLabel("TRANSAKCJE WYCHODZACE");
        transakcjeOutLabel.setBounds(50, 300, 200, 20);
        add(transakcjeOutLabel);

        outTransactionTable = new JTable();
        outTransactionScrollPane = new JScrollPane(outTransactionTable);
        outTransactionTable.setPreferredScrollableViewportSize(new Dimension(650, 180));
        outTransactionScrollPane.setBounds(10, 330, 670, 180);
        add(outTransactionScrollPane);

        rachunkiComboBox = new JComboBox<>();
        rachunkiComboBox.setBounds(10, 10, 220, 20);
        rachunkiComboBox.addActionListener(this::actionPerformed);
        List<String> rachunki = klientService.getRachunkiOfKlient(klientId);
        for (String r : rachunki)
            rachunkiComboBox.addItem(r);
        rachunkiComboBox.addItem("");
        rachunkiComboBox.setSelectedIndex(0);
        add(rachunkiComboBox);


        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(makeTransactionButton)) {
            NewTransactionFrame newTransactionFrame = new NewTransactionFrame( this, Objects.requireNonNull(rachunkiComboBox.getSelectedItem()).toString());
            this.setEnabled(false);
        }

        if (e.getSource().equals(nowyRachubekButton)) {
            if(!rachunekService.isPlaceForNewRachunek(klientId)){
                JOptionPane.showMessageDialog(
                        null,
                        "Masz za duzo utworzonych rachunkow!",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }
            else{
                Optional<Rachunek> rachunek = rachunekService.createNewRachunekForId(klientId);
                rachunek.ifPresent(r -> rachunkiComboBox.addItem(r.getId()));
                rachunkiComboBox.repaint();
                JOptionPane.showMessageDialog(
                        null,
                        "Pomyślnie utworzono nowy rachunek.",
                        "GOOD",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        }

        if (e.getSource().equals(goBackButton)) {
            klientStartFrame.setVisible(true);
            this.dispose();
        }

        if(e.getSource().equals(rachunkiComboBox)){
            if(rachunkiComboBox.getSelectedItem() != null) {
                if(!rachunkiComboBox.getSelectedItem().toString().equals("")) {
                    saldoValueLabel.setText(rachunekService.getSaldoByRachunekNumber((String) rachunkiComboBox.getSelectedItem()).toString());
                    List<Transakcja> in = transakcjaService.getInTransactionByRachunekNumber((String) rachunkiComboBox.getSelectedItem());
                    List<Transakcja> out = transakcjaService.getOutTransactionByRachunekNumber((String) rachunkiComboBox.getSelectedItem());

                    DefaultTableModel inModel = new DefaultTableModel();
                    inModel.addColumn("Tytul");
                    inModel.addColumn("Data");
                    inModel.addColumn("Kwota");
                    inModel.addColumn("Numer Nadawcy");
                    for (Transakcja t : in)
                        inModel.addRow(new Object[]{t.getTytul(), t.getData_realizacji().toString(), t.getKwota().toString(), t.getNumer_rachunku_nadawcy()});

                    DefaultTableModel outModel = new DefaultTableModel();
                    outModel.addColumn("Tytul");
                    outModel.addColumn("Data");
                    outModel.addColumn("Kwota");
                    outModel.addColumn("Numer Adresata");
                    for (Transakcja t : out)
                        outModel.addRow(new Object[]{t.getTytul(), t.getData_realizacji().toString(), t.getKwota().toString(), t.getNumer_rachunku_nadawcy()});

                    inTransactionTable.setModel(inModel);
                    outTransactionTable.setModel(outModel);
                }else{
                    saldoValueLabel.setText("-");
                }
            }

        }

    }
}
