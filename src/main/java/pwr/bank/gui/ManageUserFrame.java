package pwr.bank.gui;

import pwr.bank.model.Klient;
import pwr.bank.service.KlientService;
import pwr.bank.service.impl.DefaultKlientService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ManageUserFrame extends JFrame implements ActionListener {

    private KlientService klientService;

    private Klient klient;

    private JFrame manageUserFrame;
    private JButton goBackButton, deleteRachunekButton, editPersonalDataButton, verifyDataButton, manageMoneyButton;

    public ManageUserFrame(JFrame usersManagementFrame, Point point, Klient klient) {
        super("ZARZADZAJ");
        setLocation(point);
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        this.klientService = new DefaultKlientService();
        this.manageUserFrame = usersManagementFrame;
        this.klient = klient;

        deleteRachunekButton = new JButton("USUN RACHUNKI");
        deleteRachunekButton.setBounds(50, 50, 200, 20);
        deleteRachunekButton.addActionListener(this::actionPerformed);
        add(deleteRachunekButton);

        editPersonalDataButton = new JButton("EDYTUJ DANE KLIENTA");
        editPersonalDataButton.setBounds(50, 80, 200, 20);
        editPersonalDataButton.addActionListener(this::actionPerformed);
        add(editPersonalDataButton);

        manageMoneyButton = new JButton("TRANSAKCJE");
        manageMoneyButton.setBounds(50, 110, 200, 20);
        manageMoneyButton.addActionListener(this::actionPerformed);
        add(manageMoneyButton);

        verifyDataButton = new JButton("ZAWERYFIKUJ KONTO");
        verifyDataButton.setBounds(50, 140, 200, 20);
        verifyDataButton.addActionListener(this::actionPerformed);
        add(verifyDataButton);

        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(50, 200, 200, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(deleteRachunekButton)) {
            DeleteRachunkiFrame deleteRachunkiFrame = new DeleteRachunkiFrame(this, this.getLocation(), klient);
            this.setVisible(false);
        }

        if (e.getSource().equals(editPersonalDataButton)) {
            EditPersonalDataFrame editPersonalDataFrame = new EditPersonalDataFrame(this, this.getLocation(), klient);
            this.setVisible(false);
        }

        if (e.getSource().equals(verifyDataButton)) {
            VerifyAccountFrame verifyAccountFrame = new VerifyAccountFrame(this, this.getLocation(), klient);
            this.setVisible(false);
        }

        if (e.getSource().equals(manageMoneyButton)) {
            ManageSaldoFrame manageSaldoFrame = new ManageSaldoFrame(this, this.getLocation(), klient);
            this.setVisible(false);
        }


        if (e.getSource().equals(goBackButton)) {
            this.manageUserFrame.setVisible(true);
            this.dispose();
        }
    }
}
