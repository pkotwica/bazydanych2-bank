package pwr.bank.gui;

import pwr.bank.model.Klient;
import pwr.bank.service.KlientService;
import pwr.bank.service.impl.DefaultKlientService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class UsersManagmentFrame extends JFrame implements ActionListener {

    KlientService klientService;

    JFrame pracownikStartFrame;

    private JLabel slectUserLabel;
    private JButton manageUserButton, goBackButton;
    private JComboBox<String> usersComboBox;



    public UsersManagmentFrame(JFrame pracownikStartFrame, Point point) {
        super("ZARZADZANIE");
        setLocation(point);
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        this.pracownikStartFrame = pracownikStartFrame;
        this.klientService = new DefaultKlientService();

        slectUserLabel = new JLabel("WYBIERZ UZYTKOWNIKA: ");
        slectUserLabel.setBounds(20, 10, 200, 20);
        add(slectUserLabel);

        usersComboBox = new JComboBox<>();
        usersComboBox.setBounds(20, 40, 250, 20);
        usersComboBox.addActionListener(this::actionPerformed);
        List<String> rachunki = klientService.getKlientsAsAStringToManage();
        for (String r : rachunki)
            usersComboBox.addItem(r);
        usersComboBox.setSelectedIndex(1);
        add(usersComboBox);


        manageUserButton = new JButton("ZARZADZAJ");
        manageUserButton.setBounds(50, 110, 200, 20);
        manageUserButton.addActionListener(this::actionPerformed);
        add(manageUserButton);


        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(50, 140, 200, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);


        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource().equals(manageUserButton)){
            ManageUserFrame manageUserFrame = new ManageUserFrame(this, this.getLocation(), klientService.getKlientToManageByManagmentString(usersComboBox.getSelectedItem().toString()));
            this.setVisible(false);
        }

        if(e.getSource().equals(goBackButton)){
            pracownikStartFrame.setVisible(true);
            this.dispose();
        }
    }
}
