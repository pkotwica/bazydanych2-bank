package pwr.bank.gui;

import pwr.bank.dao.tables.KontoDao;
import pwr.bank.model.Klient;
import pwr.bank.service.KlientService;
import pwr.bank.service.KontoService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultKontoService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VerifyAccountFrame extends JFrame implements ActionListener {

    private KlientService klientService;
    private KontoService kontoService;
    private Klient klient;

    private JFrame manageUserFrame;

    private final JButton goBackButton, verifyButton;
    private final JLabel peselLabel, nameLabel, surnameLabel, nrDowoduLabel, dateOfDowodLabel, endDateOfDowodLabel, sourceOfDowodLabel;

    public VerifyAccountFrame(JFrame manageUserFrame, Point point, Klient klient) {
        super("WERYFIKACJA KONTA");
        setLocation(point);
        setSize(500, 320);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        this.klientService = new DefaultKlientService();
        this.kontoService = new DefaultKontoService();
        this.manageUserFrame = manageUserFrame;
        this.klient = klient;

        peselLabel = new JLabel("PESEL: " + klient.getId_pesel());
        peselLabel.setBounds(20, 20, 400, 20);
        add(peselLabel);

        nameLabel = new JLabel("IMIE: " + klient.getImie());
        nameLabel.setBounds(20, 50, 400, 20);
        add(nameLabel);

        surnameLabel = new JLabel("NAZWISKO: " + klient.getNazwisko());
        surnameLabel.setBounds(20, 80, 400, 20);
        add(surnameLabel);

        nrDowoduLabel = new JLabel("NUMER DOWODU: " + klient.getNumer_dowodu());
        nrDowoduLabel.setBounds(20, 110, 400, 20);
        add(nrDowoduLabel);

        dateOfDowodLabel = new JLabel("DATA WYDANIA DOWODU: " + klient.getData_wydania_dowodu().toString());
        dateOfDowodLabel.setBounds(20, 140, 400, 20);
        add(dateOfDowodLabel);

        endDateOfDowodLabel = new JLabel("DATA WAZNOSCI DOWODU: " + klient.getData_waznosci_dowodu().toString());
        endDateOfDowodLabel.setBounds(20, 170, 400, 20);
        add(endDateOfDowodLabel);

        sourceOfDowodLabel = new JLabel("ORGAN WYDANIA DOWODU: " + klient.getOrgan_wydania_dowodu());
        sourceOfDowodLabel.setBounds(20, 200, 400, 20);
        add(sourceOfDowodLabel);

        verifyButton = new JButton("DANE PRAWIDLOWE");
        if(!klientService.isVerify(klient.getId_pesel())) {
            verifyButton.setBounds(50, 230, 200, 20);
            verifyButton.addActionListener(this::actionPerformed);
            add(verifyButton);
        }
        else{
            JLabel verifiedLabel = new JLabel("KONTO ZWERYFIKOWANE");
            verifiedLabel.setBounds(60, 230, 200, 20);
            add(verifiedLabel);
        }


        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(50, 260, 200, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);


        setVisible(true);
    }



    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource().equals(verifyButton)){

            if(kontoService.verifyByKlient(klient)){
                JOptionPane.showMessageDialog(
                        null,
                        "Pomyslnie zwerfikowano klienta.",
                        "ERROR",
                        JOptionPane.INFORMATION_MESSAGE);

                this.manageUserFrame.setVisible(true);
                this.dispose();
            }else{
                JOptionPane.showMessageDialog(
                        null,
                        "Nie udalo sie zweryfikowac klienta.",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }

        }

        if(e.getSource().equals(goBackButton)){
         this.manageUserFrame.setVisible(true);
         this.dispose();
        }
    }
}
