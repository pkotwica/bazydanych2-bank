package pwr.bank.gui;

import pwr.bank.model.Klient;
import pwr.bank.model.Konto;
import pwr.bank.service.KlientService;
import pwr.bank.service.KontoService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultKontoService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

public class EditPersonalDataFrame extends JFrame implements ActionListener {

    private KlientService klientService;
    private KontoService kontoService;

    private JFrame manageUserFrame;
    private Klient klient;
    private Konto konto;

    private JLabel peselLabel, nameLabel, surnameLabel, nrDowoduLabel, dateOfDowodLabel, endDateOfDowodLabel, sourceOfDowodLabel, loginLabel, passLabel, repeatPassLabel;
    private JTextField nameField, surnameField, nrDowoduField, dateOfDowodField, endDateOfDowodField, sourceOfDowodField, loginField;
    private JPasswordField passField, repeatPassField;
    private JButton savePassButton, saveLoginButton, savePersonalDataButton, goBackButton;


    public EditPersonalDataFrame(JFrame manageUserFrame, Point point, Klient klient) {
        super("EDYTOWNIE DANYCH");
        setLocation(point);
        setSize(350, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        this.klientService = new DefaultKlientService();
        this.kontoService = new DefaultKontoService();
        this.manageUserFrame = manageUserFrame;
        this.klient = klient;
        this.konto = kontoService.getKontoByKlient(klient);


        peselLabel = new JLabel("PESEL: " + klient.getId_pesel());
        peselLabel.setBounds(20, 20, 250, 20);
        add(peselLabel);

        nameLabel = new JLabel("IMIE: ");
        nameLabel.setBounds(20, 50, 100, 20);
        add(nameLabel);

        nameField = new JTextField(klient.getImie());
        nameField.setBounds(180, 50, 150, 20);
        add(nameField);

        surnameLabel = new JLabel("NAZWISKO: ");
        surnameLabel.setBounds(20, 80, 100, 20);
        add(surnameLabel);

        surnameField = new JTextField(klient.getNazwisko());
        surnameField.setBounds(180, 80, 150, 20);
        add(surnameField);

        nrDowoduLabel = new JLabel("NUMER DOWODU: ");
        nrDowoduLabel.setBounds(20, 110, 150, 20);
        add(nrDowoduLabel);

        nrDowoduField = new JTextField(klient.getNumer_dowodu());
        nrDowoduField.setBounds(180, 110, 150, 20);
        add(nrDowoduField);

        dateOfDowodLabel = new JLabel("DATA WYDANIA DOWODU: ");
        dateOfDowodLabel.setBounds(20, 140, 250, 20);
        add(dateOfDowodLabel);

        dateOfDowodField = new JTextField(klient.getData_wydania_dowodu().toString());
        dateOfDowodField.setBounds(180, 140, 150, 20);
        add(dateOfDowodField);

        endDateOfDowodLabel = new JLabel("DATA WAZNOSCI DOWODU: ");
        endDateOfDowodLabel.setBounds(20, 170, 250, 20);
        add(endDateOfDowodLabel);

        endDateOfDowodField = new JTextField(klient.getData_waznosci_dowodu().toString());
        endDateOfDowodField.setBounds(180, 170, 150, 20);
        add(endDateOfDowodField);

        sourceOfDowodLabel = new JLabel("ORGAN WYDANIA DOWODU: ");
        sourceOfDowodLabel.setBounds(20, 200, 250, 20);
        add(sourceOfDowodLabel);

        sourceOfDowodField = new JTextField(klient.getOrgan_wydania_dowodu());
        sourceOfDowodField.setBounds(180, 200, 150, 20);
        add(sourceOfDowodField);

        savePersonalDataButton = new JButton("ZAPISZ DANE");
        savePersonalDataButton.setBounds(50, 230, 200, 20);
        savePersonalDataButton.addActionListener(this::actionPerformed);
        add(savePersonalDataButton);

        loginLabel = new JLabel("LOGIN: ");
        loginLabel.setBounds(20, 270, 100, 20);
        add(loginLabel);

        loginField = new JTextField(konto.getLogin());
        loginField.setBounds(70, 270, 150, 20);
        add(loginField);

        saveLoginButton = new JButton("ZAPISZ LOGIN");
        saveLoginButton.setBounds(50, 300, 200, 20);
        saveLoginButton.addActionListener(this::actionPerformed);
        add(saveLoginButton);

        passLabel = new JLabel("HASLO: ");
        passLabel.setBounds(20, 330, 100, 20);
        add(passLabel);

        passField = new JPasswordField("");
        passField.setBounds(100, 330, 150, 20);
        add(passField);

        repeatPassLabel = new JLabel("POWTORZ: ");
        repeatPassLabel.setBounds(20, 360, 100, 20);
        add(repeatPassLabel);

        repeatPassField = new JPasswordField("");
        repeatPassField.setBounds(100, 360, 150, 20);
        add(repeatPassField);

        savePassButton = new JButton("ZAPISZ HASLO");
        savePassButton.setBounds(50, 390, 200, 20);
        savePassButton.addActionListener(this::actionPerformed);
        add(savePassButton);

        goBackButton = new JButton("WSTECZ");
        goBackButton.setBounds(50, 430, 200, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource().equals(savePersonalDataButton)){
            boolean result = klientService.updateExistingKlient(
                              Klient.builder()
                                    .id_pesel(klient.getId_pesel())
                                    .id_konto(klient.getId_konto())
                                    .imie(nameField.getText())
                                    .nazwisko(surnameField.getText())
                                    .numer_dowodu(nrDowoduField.getText())
                                    .data_wydania_dowodu(Date.valueOf(dateOfDowodField.getText()))
                                    .data_waznosci_dowodu(Date.valueOf(endDateOfDowodField.getText()))
                                    .organ_wydania_dowodu(sourceOfDowodField.getText())
                                    .id_konto(klient.getId_konto())
                                    .build()
            );

            if(result)
                JOptionPane.showMessageDialog(
                        null,
                        "Zapisano dane personalne pomyslnie.",
                        "INFO",
                        JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(
                        null,
                        "Nie udalo sie zapisac. Cos poszlo nie tak.",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
        }

        if(e.getSource().equals(saveLoginButton)){
                boolean result = kontoService.updateExistingKonto(
                        Konto.builder()
                                .id(konto.getId())
                                .login(loginField.getText())
                                .liczba_rachunkow(konto.getLiczba_rachunkow())
                                .haslo(konto.getHaslo())
                                .weryfikacja(konto.getWeryfikacja())
                                .build()
                );
                this.konto = kontoService.getKontoByKlient(klient);

                if (result)
                    JOptionPane.showMessageDialog(
                            null,
                            "Zapisano login pomyslnie.",
                            "INFO",
                            JOptionPane.INFORMATION_MESSAGE);
                else
                    JOptionPane.showMessageDialog(
                            null,
                            "Nie udalo sie zapisac. Cos poszlo nie tak.\nTaki login moze juz istniec.",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
        }

        if(e.getSource().equals(savePassButton)){
            if(passField.getText().equals(repeatPassField.getText())) {
                boolean result = kontoService.updateExistingKonto(
                        Konto.builder()
                                .id(konto.getId())
                                .login(konto.getLogin())
                                .liczba_rachunkow(konto.getLiczba_rachunkow())
                                .haslo(passField.getText())
                                .weryfikacja(konto.getWeryfikacja())
                                .build()
                );
                this.konto = kontoService.getKontoByKlient(klient);

                if (result)
                    JOptionPane.showMessageDialog(
                            null,
                            "Zapisano haslo pomyslnie.",
                            "INFO",
                            JOptionPane.INFORMATION_MESSAGE);
                else
                    JOptionPane.showMessageDialog(
                            null,
                            "Nie udalo sie zapisac. Cos poszlo nie tak.",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
            }else {
                JOptionPane.showMessageDialog(
                        null,
                        "Nie powtorzono poprawnie hasla.",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

        if(e.getSource().equals(goBackButton)){
            this.manageUserFrame.setVisible(true);
            this.dispose();
        }
    }
}
