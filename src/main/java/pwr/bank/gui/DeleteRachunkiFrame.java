package pwr.bank.gui;

import javafx.scene.control.ComboBox;
import pwr.bank.model.Klient;
import pwr.bank.model.Transakcja;
import pwr.bank.service.KlientService;
import pwr.bank.service.RachunekService;
import pwr.bank.service.impl.DefaultKlientService;
import pwr.bank.service.impl.DefaultRachunekService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeleteRachunkiFrame extends JFrame implements ActionListener {

    private String GET_MONEY_DECISON = "Wyplac";
    private String MOVE_MONEY_DECISION = "Przelej";

    private KlientService klientService;
    private RachunekService rachunekService;
    private Klient klient;
    private JFrame manageUserFrame;
    private JTextField accountNumberField;

    private JLabel saldoLabel, saldoValueLabel, decisionLabel, accountNumberLabel;
    private JComboBox<String> rachunkiComboBox, decisionComboBox;
    private JButton deleteButton, goBackButton;

    public DeleteRachunkiFrame(JFrame manageUserFrame, Point point, Klient klient) {
        super("USUWANIE RACHUNKOW");
        setLocation(point);
        setSize(300, 310);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        this.manageUserFrame = manageUserFrame;
        this.klientService = new DefaultKlientService();
        this.rachunekService = new DefaultRachunekService();
        this.klient = klient;
        List<String> decisions = new ArrayList<>(Arrays.asList(GET_MONEY_DECISON, MOVE_MONEY_DECISION));

        saldoValueLabel = new JLabel("");
        saldoValueLabel.setBounds(150, 40, 100, 20);
        add(saldoValueLabel);

        saldoLabel = new JLabel("SALDO: ");
        saldoLabel.setBounds(20, 40, 100, 20);
        add(saldoLabel);

        decisionLabel = new JLabel("OBECNY STAN KONTA: ");
        decisionLabel.setBounds(20, 80, 150, 20);
        add(decisionLabel);

        decisionComboBox = new JComboBox<>();
        decisionComboBox.setBounds(20, 100, 220, 20);
        for (String d : decisions)
            decisionComboBox.addItem(d);
        decisionComboBox.setSelectedIndex(1);
        decisionComboBox.addActionListener(this::actionPerformed);
        add(decisionComboBox);

        accountNumberLabel = new JLabel("NUMER DO PRZELEWU: ");
        accountNumberLabel.setBounds(20, 140, 150, 20);
        add(accountNumberLabel);

        accountNumberField = new JTextField("");
        accountNumberField.setBounds(20, 170, 200, 20);
        add(accountNumberField);

        deleteButton = new JButton("USUN WYBRANY RACHUNEK");
        deleteButton.setBounds(50, 220, 200, 20);
        deleteButton.addActionListener(this::actionPerformed);
        add(deleteButton);

        goBackButton = new JButton("ANULUJ");
        goBackButton.setBounds(50, 250, 200, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        rachunkiComboBox = new JComboBox<>();
        rachunkiComboBox.setBounds(10, 10, 220, 20);
        rachunkiComboBox.addActionListener(this::actionPerformed);
        List<String> rachunki = klientService.getRachunkiOfKlient(klient.getId_pesel());
        for (String r : rachunki)
            rachunkiComboBox.addItem(r);
        rachunkiComboBox.addItem("");
        rachunkiComboBox.setSelectedIndex(0);
        add(rachunkiComboBox);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource().equals(rachunkiComboBox) && rachunkiComboBox.getSelectedItem() != null){
            if(rachunkiComboBox.getSelectedItem().toString().equals("")){
                saldoValueLabel.setText("");
            }else {
                saldoValueLabel.setText(rachunekService.getSaldoByRachunekNumber((String) rachunkiComboBox.getSelectedItem()).toString());
            }
        }

        if(e.getSource().equals(decisionComboBox) && decisionComboBox.getSelectedItem() != null){
            if(decisionComboBox.getSelectedItem().toString().equals(MOVE_MONEY_DECISION)){
                accountNumberField.setEnabled(true);
            }else{
                accountNumberField.setEnabled(false);
            }
        }

        if(e.getSource().equals(deleteButton)){
            if(decisionComboBox.getSelectedItem().toString().equals(MOVE_MONEY_DECISION) && accountNumberField.getText().equals("")){
                JOptionPane.showMessageDialog(
                        null,
                        "Dla wybranej decyzji, musisz podac numer rachunku.",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }else{
                int decision = JOptionPane.showConfirmDialog(
                        null,
                        "Czy jestes pewien ze chesz usunac wybrany rachunek?",
                        "CZY NA PEWNO",
                        JOptionPane.YES_NO_OPTION);
                if(decision == 0) {
                    if (decisionComboBox.getSelectedItem().toString().equals(MOVE_MONEY_DECISION)) {
                        rachunekService.deleteRahunekByNumber(rachunkiComboBox.getSelectedItem().toString(), accountNumberField.getText());
                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Wyplac pieniadze.",
                                "WYPLATA",
                                JOptionPane.INFORMATION_MESSAGE);
                        rachunekService.deleteRahunekByNumber(rachunkiComboBox.getSelectedItem().toString());
                    }
                }
            }
        }

        if(e.getSource().equals(rachunkiComboBox)){
            if(rachunkiComboBox.getSelectedItem().toString().equals(GET_MONEY_DECISON)){
                accountNumberLabel.setEnabled(false);
                accountNumberField.setEnabled(false);
            }

            if(rachunkiComboBox.getSelectedItem().toString().equals(MOVE_MONEY_DECISION)){
                accountNumberLabel.setEnabled(true);
                accountNumberField.setEnabled(true);
            }
        }

        if(e.getSource().equals(goBackButton)){
            manageUserFrame.setVisible(true);
            this.dispose();
        }
    }
}
