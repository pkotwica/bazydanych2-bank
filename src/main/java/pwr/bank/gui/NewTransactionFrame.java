package pwr.bank.gui;

import pwr.bank.model.Adres;
import pwr.bank.service.AdresService;
import pwr.bank.service.RachunekService;
import pwr.bank.service.TransakcjaService;
import pwr.bank.service.impl.DefaultAdresService;
import pwr.bank.service.impl.DefaultRachunekService;
import pwr.bank.service.impl.DefaultTransakcjaService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class NewTransactionFrame extends JFrame implements ActionListener {

    private AdresService adresService;
    private RachunekService rachunekService;
    private TransakcjaService transakcjaService;

    private String rachunekNumber;

    private JFrame rachunkiFrame;
    private JLabel tytulLabel, rachunekLabel, adresyLabel, adresLabel, miastoLabel, ulicaLabel, nrLabel, kwotaLabel, walutaLabel, rodzajLabel, dotLabel;
    private JTextField tytulTextField, rachunekTextField, miastoTextField, ulicaTextField, nrTextField, calosciTextField, ulamekTextField;
    private JComboBox<String> adresyComboBox, rodzajComboBox;

    private JButton goBackButton, makeButton;


    public NewTransactionFrame(JFrame rachunkiFrame, String rachunekNumber) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(600, 500);
        setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setResizable(false);

        this.adresService = new DefaultAdresService();
        this.rachunekService = new DefaultRachunekService();
        this.transakcjaService = new DefaultTransakcjaService();
        this.rachunkiFrame = rachunkiFrame;
        this.rachunekNumber = rachunekNumber;

        tytulLabel = new JLabel("TYTUL PRZELEWU: ");
        tytulLabel.setBounds(50, 10, 200, 20);
        add(tytulLabel);

        tytulTextField = new JTextField("Przelew srodkow");
        tytulTextField.setBounds(20, 40, 300, 20);
        add(tytulTextField);

        rachunekLabel = new JLabel("NUMER RACHUNKU: ");
        rachunekLabel.setBounds(50, 70, 200, 20);
        add(rachunekLabel);

        rachunekTextField = new JTextField("");
        rachunekTextField.setBounds(20, 100, 250, 20);
        add(rachunekTextField);

        adresyLabel = new JLabel("ZNANE NUMERY");
        adresyLabel.setBounds(280, 70, 200, 20);
        add(adresyLabel);

        adresyComboBox = new JComboBox<>();
        adresyComboBox.setBounds(280, 100, 170, 20);
        adresyComboBox.addActionListener(this::actionPerformed);
        adresyComboBox.addItem("");
        List<String> adresy = adresService.getRachunkiOfKlient(rachunekNumber);
        for (String r : adresy)
            adresyComboBox.addItem(r);
        add(adresyComboBox);

        adresLabel = new JLabel("ADRES");
        adresLabel.setBounds(50, 130, 200, 20);
        add(adresLabel);

        miastoLabel = new JLabel("MIASTO: ");
        miastoLabel.setBounds(20, 160, 200, 20);
        add(miastoLabel);

        miastoTextField = new JTextField("");
        miastoTextField.setBounds(70, 160, 100, 20);
        add(miastoTextField);

        ulicaLabel = new JLabel("ULICA: ");
        ulicaLabel.setBounds(180, 160, 200, 20);
        add(ulicaLabel);

        ulicaTextField = new JTextField("");
        ulicaTextField.setBounds(220, 160, 100, 20);
        add(ulicaTextField);

        nrLabel = new JLabel("NR: ");
        nrLabel.setBounds(340, 160, 50, 20);
        add(nrLabel);

        nrTextField = new JTextField("");
        nrTextField.setBounds(370, 160, 50, 20);
        add(nrTextField);

        kwotaLabel = new JLabel("KWOTA: ");
        kwotaLabel.setBounds(50, 190, 200, 20);
        add(kwotaLabel);

        walutaLabel = new JLabel(" PLN");
        walutaLabel.setBounds(130, 220, 200, 20);
        add(walutaLabel);

        calosciTextField = new JTextField("");
        calosciTextField.setBounds(20, 220, 70, 20);
        add(calosciTextField);

        dotLabel = new JLabel(".");
        dotLabel.setBounds(95, 220, 10, 20);
        add(dotLabel);

        ulamekTextField = new JTextField("");
        ulamekTextField.setBounds(100, 220, 30, 20);
        add(ulamekTextField);

        rodzajLabel = new JLabel("RODZAJ PRZELEWU: ");
        rodzajLabel.setBounds(50, 250, 200, 20);
        add(rodzajLabel);

        rodzajComboBox = new JComboBox<>();
        rodzajComboBox.setBounds(20, 280, 150, 20);
        rodzajComboBox.addItem("ZWYKLY");
        rodzajComboBox.addItem("SZYBKI (+10 PLN)");
        add(rodzajComboBox);


        goBackButton = new JButton("ANULUJ");
        goBackButton.setBounds(30, 350, 120, 20);
        goBackButton.addActionListener(this::actionPerformed);
        add(goBackButton);

        makeButton = new JButton("WYKONAJ PRZELEW");
        makeButton.setBounds(350, 350, 120, 20);
        makeButton.addActionListener(this::actionPerformed);
        add(makeButton);


        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource().equals(goBackButton)){
            rachunkiFrame.setEnabled(true);
            this.dispose();
        }

        if(e.getSource().equals(makeButton)){

            if(!tytulTextField.getText().isEmpty() && !rachunekTextField.getText().isEmpty() && !miastoTextField.getText().isEmpty() &&
                    !ulicaTextField.getText().isEmpty() && !nrTextField.getText().isEmpty() && !calosciTextField.getText().isEmpty() && !ulamekTextField.getText().isEmpty()){
               if(Integer.parseInt(calosciTextField.getText())>0 && Integer.parseInt(ulamekTextField.getText())>=0) {

                    if (rachunekTextField.getText().length() == 26) {
                        if (rachunekService.checkEnoughMoney(rachunekNumber, calosciTextField.getText() + "." + ulamekTextField.getText())) {
                            if (transakcjaService.makeTransaction(tytulTextField.getText(), miastoTextField.getSelectedText() + ",ul." + ulicaTextField.getText() + nrTextField.getText(), calosciTextField.getText() + "." + ulamekTextField.getText(), rodzajComboBox.getSelectedItem().toString(), rachunekTextField.getText(), rachunekNumber)) {
                                rachunkiFrame.setEnabled(true);
                                this.dispose();
                            } else {
                                JOptionPane.showMessageDialog(
                                        null,
                                        "Cos poszlo nie tak. Sprawdz podane dane.",
                                        "ERROR",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Nie masz wystarczajaco pieniedzy na wybranym rachunku!",
                                    "ERROR",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Podaj poprawny numer rachunku!",
                                "ERROR",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(
                            null,
                            "Kwota musi byc większa od 0.00 PLN.",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                }

            }else{
                JOptionPane.showMessageDialog(
                        null,
                        "Wszystkie pola wymagaja uzupelnienia!",
                        "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }

        }

        if(e.getSource().equals(adresyComboBox)){
            if(adresyComboBox.getSelectedItem().equals(""))
                rachunekTextField.setText("");
            else{
                Adres adres = adresService.findAddressByNameAndOwner(adresyComboBox.getSelectedItem().toString(), rachunekNumber);
                rachunekTextField.setText(adres.getNr_rachunku());
            }
        }

    }
}
